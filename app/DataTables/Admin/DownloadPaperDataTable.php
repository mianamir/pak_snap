<?php

namespace App\DataTables\Admin;

use App\Models\Admin\DownloadPaper;
use Form;
use Yajra\Datatables\Services\DataTable;

class DownloadPaperDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'admin.download_papers.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $downloadPapers = DownloadPaper::query();

        return $this->applyScopes($downloadPapers);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'category' => ['name' => 'category', 'data' => 'category'],
            'class' => ['name' => 'class', 'data' => 'class'],
            'subject' => ['name' => 'subject', 'data' => 'subject'],
            'demo_file' => ['name' => 'demo_file', 'data' => 'demo_file'],
            'original_file' => ['name' => 'original_file', 'data' => 'original_file'],
            'type' => ['name' => 'type', 'data' => 'type'],
            'chapter_or_term' => ['name' => 'chapter_or_term', 'data' => 'chapter_or_term'],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'downloadPapers';
    }
}
