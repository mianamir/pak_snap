<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateChildPagesRequest;
use App\Http\Requests\Admin\UpdateChildPagesRequest;
use App\Repositories\Admin\ChildPagesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ChildPagesController extends AppBaseController
{
    /** @var  ChildPagesRepository */
    private $childPagesRepository;

    public function __construct(ChildPagesRepository $childPagesRepo)
    {
        $this->childPagesRepository = $childPagesRepo;
    }

    /**
     * Display a listing of the ChildPages.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->childPagesRepository->pushCriteria(new RequestCriteria($request));
        $childPages = $this->childPagesRepository->all();

        return view('admin.child_pages.index')
            ->with('childPages', $childPages);
    }

    /**
     * Show the form for creating a new ChildPages.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.child_pages.create');
    }

    /**
     * Store a newly created ChildPages in storage.
     *
     * @param CreateChildPagesRequest $request
     *
     * @return Response
     */
    public function store(CreateChildPagesRequest $request)
    {
        $input = $request->all();

        $childPages = $this->childPagesRepository->create($input);


        if ($request->file('image')) {
            $data = \Imageupload::upload($request->file('image'));
            $childPages->image = $data['original_filedir'];
            $childPages->update();
        }

        Flash::success('Child Pages saved successfully.');

        return redirect(route('admin.childPages.index'));
    }

    /**
     * Display the specified ChildPages.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $childPages = $this->childPagesRepository->findWithoutFail($id);

        if (empty($childPages)) {
            Flash::error('Child Pages not found');

            return redirect(route('admin.childPages.index'));
        }

        return view('admin.child_pages.show')->with('childPages', $childPages);
    }

    /**
     * Show the form for editing the specified ChildPages.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $childPages = $this->childPagesRepository->findWithoutFail($id);

        if (empty($childPages)) {
            Flash::error('Child Pages not found');

            return redirect(route('admin.childPages.index'));
        }

        return view('admin.child_pages.edit')->with('childPages', $childPages);
    }

    /**
     * Update the specified ChildPages in storage.
     *
     * @param  int              $id
     * @param UpdateChildPagesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateChildPagesRequest $request)
    {
        $childPages = $this->childPagesRepository->findWithoutFail($id);

        if (empty($childPages)) {
            Flash::error('Child Pages not found');

            return redirect(route('admin.childPages.index'));
        }

        $childPages = $this->childPagesRepository->update($request->all(), $id);

        if ($request->file('image')) {
            $data = \Imageupload::upload($request->file('image'));
            $childPages->image = $data['original_filedir'];
            $childPages->update();
        }

        Flash::success('Child Pages updated successfully.');

        return redirect(route('admin.childPages.index'));
    }

    /**
     * Remove the specified ChildPages from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $childPages = $this->childPagesRepository->findWithoutFail($id);

        if (empty($childPages)) {
            Flash::error('Child Pages not found');

            return redirect(route('admin.childPages.index'));
        }

        $this->childPagesRepository->delete($id);

        Flash::success('Child Pages deleted successfully.');

        return redirect(route('admin.childPages.index'));
    }
}
