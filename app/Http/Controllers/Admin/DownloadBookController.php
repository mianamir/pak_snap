<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\DownloadBookDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateDownloadBookRequest;
use App\Http\Requests\Admin\UpdateDownloadBookRequest;
use App\Repositories\Admin\DownloadBookRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class DownloadBookController extends AppBaseController
{
    /** @var  DownloadBookRepository */
    private $downloadBookRepository;

    public function __construct(DownloadBookRepository $downloadBookRepo)
    {
        $this->downloadBookRepository = $downloadBookRepo;
    }

    /**
     * Display a listing of the DownloadBook.
     *
     * @param DownloadBookDataTable $downloadBookDataTable
     * @return Response
     */
    public function index(DownloadBookDataTable $downloadBookDataTable)
    {
        return $downloadBookDataTable->render('admin.download_books.index');
    }

    /**
     * Show the form for creating a new DownloadBook.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.download_books.create');
    }

    /**
     * Store a newly created DownloadBook in storage.
     *
     * @param CreateDownloadBookRequest $request
     *
     * @return Response
     */
    public function store(CreateDownloadBookRequest $request)
    {
        $input = $request->all();

        $downloadBook = $this->downloadBookRepository->create($input);



        if ($request->file('image')) {

            $data = \Imageupload::upload($request->file('image'));
            $downloadBook->image = $data['original_filedir'];
            $downloadBook->save();

        }

        $file = $request->file('demo_file');
        if ($request->file('demo_file')) {
            $uniqueFileName = $request->file('demo_file')->getClientOriginalName();

            $request->file('demo_file')->move(public_path('files'), $uniqueFileName);
            $downloadBook->demo_file = 'files/' . $uniqueFileName;
            $downloadBook->save();
        }


        if ($request->file('original_file')) {
            $uniqueFileName = $request->file('original_file')->getClientOriginalName();
            $request->file('original_file')->move(public_path('files'), $uniqueFileName);
            $downloadBook->original_file = 'files/' . $uniqueFileName;
            $downloadBook->save();
        }

        \Flash::success('Download Book saved successfully.');

        return redirect(route('admin.downloadBooks.index'));
    }


    /**
     * Display the specified DownloadBook.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $downloadBook = $this->downloadBookRepository->findWithoutFail($id);

        if (empty($downloadBook)) {
            Flash::error('Download Book not found');

            return redirect(route('admin.downloadBooks.index'));
        }

        return view('admin.download_books.show')->with('downloadBook', $downloadBook);
    }

    /**
     * Show the form for editing the specified DownloadBook.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $downloadBook = $this->downloadBookRepository->findWithoutFail($id);

        if (empty($downloadBook)) {
            Flash::error('Download Book not found');

            return redirect(route('admin.downloadBooks.index'));
        }

        return view('admin.download_books.edit')->with('downloadBook', $downloadBook);
    }

    /**
     * Update the specified DownloadBook in storage.
     *
     * @param  int $id
     * @param UpdateDownloadBookRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDownloadBookRequest $request)
    {
        $downloadBook = $this->downloadBookRepository->findWithoutFail($id);

        if (empty($downloadBook)) {
            Flash::error('Download Book not found');

            return redirect(route('admin.downloadBooks.index'));
        }

        $downloadBook = $this->downloadBookRepository->update($request->all(), $id);



        if ($request->file('image')) {

            $data = \Imageupload::upload($request->file('image'));
            $downloadBook->image = $data['original_filedir'];
            $downloadBook->save();

        }


        $file = $request->file('demo_file');
        if ($request->file('demo_file')) {
            $uniqueFileName = $request->file('demo_file')->getClientOriginalName();
            $request->file('demo_file')->move(public_path('files'), $uniqueFileName);
            $downloadBook->demo_file = 'files/' . $uniqueFileName;
            $downloadBook->save();
        }

        if ($request->file('original_file')) {
            $uniqueFileName = $request->file('original_file')->getClientOriginalName();
            $request->file('original_file')->move(public_path('files'), $uniqueFileName);
            $downloadBook->original_file = 'files/' . $uniqueFileName;
            $downloadBook->save();
        }

        Flash::success('Download Book updated successfully.');

        return redirect(route('admin.downloadBooks.index'));
    }

    /**
     * Remove the specified DownloadBook from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $downloadBook = $this->downloadBookRepository->findWithoutFail($id);

        if (empty($downloadBook)) {
            Flash::error('Download Book not found');

            return redirect(route('admin.downloadBooks.index'));
        }

        $this->downloadBookRepository->delete($id);

        Flash::success('Download Book deleted successfully.');

        return redirect(route('admin.downloadBooks.index'));
    }
}
