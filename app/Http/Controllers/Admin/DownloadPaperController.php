<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\DownloadPaperDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateDownloadPaperRequest;
use App\Http\Requests\Admin\UpdateDownloadPaperRequest;
use App\Repositories\Admin\DownloadPaperRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class DownloadPaperController extends AppBaseController
{
    /** @var  DownloadPaperRepository */
    private $downloadPaperRepository;

    public function __construct(DownloadPaperRepository $downloadPaperRepo)
    {
        $this->downloadPaperRepository = $downloadPaperRepo;
    }

    /**
     * Display a listing of the DownloadPaper.
     *
     * @param DownloadPaperDataTable $downloadPaperDataTable
     * @return Response
     */
    public function index(DownloadPaperDataTable $downloadPaperDataTable)
    {
        return $downloadPaperDataTable->render('admin.download_papers.index');
    }

    /**
     * Show the form for creating a new DownloadPaper.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.download_papers.create');
    }

    /**
     * Store a newly created DownloadPaper in storage.
     *
     * @param CreateDownloadPaperRequest $request
     *
     * @return Response
     */
    public function store(CreateDownloadPaperRequest $request)
    {
        $input = $request->all();

        $downloadPaper = $this->downloadPaperRepository->create($input);

        if ($request->file('image')) {
            $data = \Imageupload::upload($request->file('image'));
            $downloadPaper->image = $data['original_filedir'];
            $downloadPaper->save();

        }


        if ($request->file('demo_file')) {
            $uniqueFileName = $request->file('demo_file')->getClientOriginalName();
            $request->file('demo_file')->move(public_path('files'), $uniqueFileName);
            $downloadPaper->demo_file = 'files/' . $uniqueFileName;
            $downloadPaper->save();
        }

        if ($request->file('original_file')) {
            $uniqueFileName = $request->file('original_file')->getClientOriginalName();
            $request->file('original_file')->move(public_path('files'), $uniqueFileName);
            $downloadPaper->original_file = 'files/' . $uniqueFileName;
            $downloadPaper->save();
        }


        Flash::success('Download Paper saved successfully.');

        return redirect(route('admin.downloadPapers.index'));
    }

    /**
     * Display the specified DownloadPaper.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $downloadPaper = $this->downloadPaperRepository->findWithoutFail($id);

        if (empty($downloadPaper)) {
            Flash::error('Download Paper not found');

            return redirect(route('admin.downloadPapers.index'));
        }

        return view('admin.download_papers.show')->with('downloadPaper', $downloadPaper);
    }

    /**
     * Show the form for editing the specified DownloadPaper.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $downloadPaper = $this->downloadPaperRepository->findWithoutFail($id);

        if (empty($downloadPaper)) {
            Flash::error('Download Paper not found');

            return redirect(route('admin.downloadPapers.index'));
        }

        return view('admin.download_papers.edit')->with('downloadPaper', $downloadPaper);
    }

    /**
     * Update the specified DownloadPaper in storage.
     *
     * @param  int              $id
     * @param UpdateDownloadPaperRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDownloadPaperRequest $request)
    {
        $downloadPaper = $this->downloadPaperRepository->findWithoutFail($id);

        if (empty($downloadPaper)) {
            Flash::error('Download Paper not found');

            return redirect(route('admin.downloadPapers.index'));
        }

        $downloadPaper = $this->downloadPaperRepository->update($request->all(), $id);


        if ($request->file('image')) {

            $data = \Imageupload::upload($request->file('image'));
            $downloadPaper->image = $data['original_filedir'];
            $downloadPaper->save();

        }



        if ($request->file('demo_file')) {
            $uniqueFileName = $request->file('demo_file')->getClientOriginalName();
            $request->file('demo_file')->move(public_path('files'), $uniqueFileName);
            $downloadPaper->demo_file = 'files/' . $uniqueFileName;
            $downloadPaper->save();
        }

        if ($request->file('original_file')) {
            $uniqueFileName = $request->file('original_file')->getClientOriginalName();
            $request->file('original_file')->move(public_path('files'), $uniqueFileName);
            $downloadPaper->original_file = 'files/' . $uniqueFileName;
            $downloadPaper->save();
        }



        Flash::success('Download Paper updated successfully.');

        return redirect(route('admin.downloadPapers.index'));
    }

    /**
     * Remove the specified DownloadPaper from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $downloadPaper = $this->downloadPaperRepository->findWithoutFail($id);

        if (empty($downloadPaper)) {
            Flash::error('Download Paper not found');

            return redirect(route('admin.downloadPapers.index'));
        }

        $this->downloadPaperRepository->delete($id);

        Flash::success('Download Paper deleted successfully.');

        return redirect(route('admin.downloadPapers.index'));
    }
}
