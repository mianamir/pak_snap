<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\ProjectDetailDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateProjectDetailRequest;
use App\Http\Requests\Admin\UpdateProjectDetailRequest;
use App\Repositories\Admin\ProjectDetailRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ProjectDetailController extends AppBaseController
{
    /** @var  ProjectDetailRepository */
    private $projectDetailRepository;

    public function __construct(ProjectDetailRepository $projectDetailRepo)
    {
        $this->projectDetailRepository = $projectDetailRepo;
    }

    /**
     * Display a listing of the ProjectDetail.
     *
     * @param ProjectDetailDataTable $projectDetailDataTable
     * @return Response
     */
    public function index(ProjectDetailDataTable $projectDetailDataTable)
    {
        return $projectDetailDataTable->render('admin.project_details.index');
    }

    /**
     * Show the form for creating a new ProjectDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.project_details.create');
    }

    /**
     * Store a newly created ProjectDetail in storage.
     *
     * @param CreateProjectDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateProjectDetailRequest $request)
    {
        $input = $request->all();

        $projectDetail = $this->projectDetailRepository->create($input);

        Flash::success('Project Detail saved successfully.');

        return redirect(route('admin.projectDetails.index'));
    }

    /**
     * Display the specified ProjectDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $projectDetail = $this->projectDetailRepository->findWithoutFail($id);

        if (empty($projectDetail)) {
            Flash::error('Project Detail not found');

            return redirect(route('admin.projectDetails.index'));
        }

        return view('admin.project_details.show')->with('projectDetail', $projectDetail);
    }

    /**
     * Show the form for editing the specified ProjectDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $projectDetail = $this->projectDetailRepository->findWithoutFail($id);

        if (empty($projectDetail)) {
            Flash::error('Project Detail not found');

            return redirect(route('admin.projectDetails.index'));
        }

        return view('admin.project_details.edit')->with('projectDetail', $projectDetail);
    }

    /**
     * Update the specified ProjectDetail in storage.
     *
     * @param  int              $id
     * @param UpdateProjectDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProjectDetailRequest $request)
    {
        $projectDetail = $this->projectDetailRepository->findWithoutFail($id);

        if (empty($projectDetail)) {
            Flash::error('Project Detail not found');

            return redirect(route('admin.projectDetails.index'));
        }

        $projectDetail = $this->projectDetailRepository->update($request->all(), $id);

        Flash::success('Project Detail updated successfully.');

        return redirect(route('admin.projectDetails.index'));
    }

    /**
     * Remove the specified ProjectDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $projectDetail = $this->projectDetailRepository->findWithoutFail($id);

        if (empty($projectDetail)) {
            Flash::error('Project Detail not found');

            return redirect(route('admin.projectDetails.index'));
        }

        $this->projectDetailRepository->delete($id);

        Flash::success('Project Detail deleted successfully.');

        return redirect(route('admin.projectDetails.index'));
    }
}
