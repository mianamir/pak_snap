<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateQuickLinkRequest;
use App\Http\Requests\Admin\UpdateQuickLinkRequest;
use App\Repositories\Admin\QuickLinkRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class QuickLinkController extends AppBaseController
{
    /** @var  QuickLinkRepository */
    private $quickLinkRepository;

    public function __construct(QuickLinkRepository $quickLinkRepo)
    {
        $this->quickLinkRepository = $quickLinkRepo;
    }

    /**
     * Display a listing of the QuickLink.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->quickLinkRepository->pushCriteria(new RequestCriteria($request));
        $quickLinks = $this->quickLinkRepository->all();

        return view('admin.quick_links.index')
            ->with('quickLinks', $quickLinks);
    }

    /**
     * Show the form for creating a new QuickLink.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.quick_links.create');
    }

    /**
     * Store a newly created QuickLink in storage.
     *
     * @param CreateQuickLinkRequest $request
     *
     * @return Response
     */
    public function store(CreateQuickLinkRequest $request)
    {
        $input = $request->all();

        $quickLink = $this->quickLinkRepository->create($input);

        Flash::success('Quick Link saved successfully.');

        return redirect(route('admin.quickLinks.index'));
    }

    /**
     * Display the specified QuickLink.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $quickLink = $this->quickLinkRepository->findWithoutFail($id);

        if (empty($quickLink)) {
            Flash::error('Quick Link not found');

            return redirect(route('admin.quickLinks.index'));
        }

        return view('admin.quick_links.show')->with('quickLink', $quickLink);
    }

    /**
     * Show the form for editing the specified QuickLink.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $quickLink = $this->quickLinkRepository->findWithoutFail($id);

        if (empty($quickLink)) {
            Flash::error('Quick Link not found');

            return redirect(route('admin.quickLinks.index'));
        }

        return view('admin.quick_links.edit')->with('quickLink', $quickLink);
    }

    /**
     * Update the specified QuickLink in storage.
     *
     * @param  int              $id
     * @param UpdateQuickLinkRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQuickLinkRequest $request)
    {
        $quickLink = $this->quickLinkRepository->findWithoutFail($id);

        if (empty($quickLink)) {
            Flash::error('Quick Link not found');

            return redirect(route('admin.quickLinks.index'));
        }

        $quickLink = $this->quickLinkRepository->update($request->all(), $id);

        Flash::success('Quick Link updated successfully.');

        return redirect(route('admin.quickLinks.index'));
    }

    /**
     * Remove the specified QuickLink from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $quickLink = $this->quickLinkRepository->findWithoutFail($id);

        if (empty($quickLink)) {
            Flash::error('Quick Link not found');

            return redirect(route('admin.quickLinks.index'));
        }

        $this->quickLinkRepository->delete($id);

        Flash::success('Quick Link deleted successfully.');

        return redirect(route('admin.quickLinks.index'));
    }
}
