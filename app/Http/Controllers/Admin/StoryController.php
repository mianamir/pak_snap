<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateStoryRequest;
use App\Http\Requests\Admin\UpdateStoryRequest;
use App\Repositories\Admin\StoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class StoryController extends AppBaseController
{
    /** @var  StoryRepository */
    private $storyRepository;

    public function __construct(StoryRepository $storyRepo)
    {
        $this->storyRepository = $storyRepo;
    }

    /**
     * Display a listing of the Story.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->storyRepository->pushCriteria(new RequestCriteria($request));
        $stories = $this->storyRepository->all();

        return view('admin.stories.index')
            ->with('stories', $stories);
    }

    /**
     * Show the form for creating a new Story.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.stories.create');
    }

    /**
     * Store a newly created Story in storage.
     *
     * @param CreateStoryRequest $request
     *
     * @return Response
     */
    public function store(CreateStoryRequest $request)
    {
        $input = $request->all();

        $story = $this->storyRepository->create($input);

        if ($request->file('image')) {
            $data = \Imageupload::upload($request->file('image'));
            $story->image = $data['original_filedir'];
            $story->save();
        }


        Flash::success('Story saved successfully.');

        return redirect(route('admin.stories.index'));
    }

    /**
     * Display the specified Story.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $story = $this->storyRepository->findWithoutFail($id);

        if (empty($story)) {
            Flash::error('Story not found');

            return redirect(route('admin.stories.index'));
        }

        return view('admin.stories.show')->with('story', $story);
    }

    /**
     * Show the form for editing the specified Story.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $story = $this->storyRepository->findWithoutFail($id);

        if (empty($story)) {
            Flash::error('Story not found');

            return redirect(route('admin.stories.index'));
        }

        return view('admin.stories.edit')->with('story', $story);
    }

    /**
     * Update the specified Story in storage.
     *
     * @param  int              $id
     * @param UpdateStoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoryRequest $request)
    {
        $story = $this->storyRepository->findWithoutFail($id);

        if (empty($story)) {
            Flash::error('Story not found');

            return redirect(route('admin.stories.index'));
        }

        $story = $this->storyRepository->update($request->all(), $id);

        if ($request->file('image')) {
            $data = \Imageupload::upload($request->file('image'));
            $story->image = $data['original_filedir'];
            $story->save();
        }



        Flash::success('Story updated successfully.');

        return redirect(route('admin.stories.index'));
    }

    /**
     * Remove the specified Story from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $story = $this->storyRepository->findWithoutFail($id);

        if (empty($story)) {
            Flash::error('Story not found');

            return redirect(route('admin.stories.index'));
        }

        $this->storyRepository->delete($id);

        Flash::success('Story deleted successfully.');

        return redirect(route('admin.stories.index'));
    }
}
