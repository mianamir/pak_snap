<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Admin\AirAccessories;
use App\Models\Admin\AirCompressors;
use App\Models\Admin\Book;
use App\Models\Admin\Category;
use App\Models\Admin\DownloadBook;
use App\Models\Admin\DownloadPaper;
use App\Models\Admin\Gallery;
use App\Models\Admin\Page;
use App\Models\Admin\Product;
use App\Models\Admin\Story;
use App\Models\Admin\SubCategory;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use  \App\Lib\PDF;

/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class FrontendController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $page = \App\Models\Admin\Page::where('name', '=', 'home')->first();
        \Meta::set('title', $page->title);
        \Meta::set('description', $page->meta_description);
        \Meta::set('image', asset($page->image));

        return view('frontend.site.index');
    }

    public function profile()
    {
        $page = Page::where('name', 'profile')->first();

        return view('frontend.site.profile',compact('page'));
    }
    public function service()
    {
        $page = Page::where('name', 'services')->first();

        return view('frontend.site.service',compact('page'));
    }
    public function privacy()
    {
        $page = Page::where('name', 'privacy')->first();

        return view('frontend.site.privacy',compact('page'));
    }
    public function term()
    {
        $page = Page::where('name', 'terms')->first();

        return view('frontend.site.term',compact('page'));
    }

    public function contact()
    {
        $page = Page::where('name', 'contact')->first();

        return view('frontend.site.contact',compact('page'));
    }




    public function page($slug)
    {
        $data = \App\Models\Admin\Page::findBySlug($slug);
        \Meta::set('title', $data->title);
        \Meta::set('description', $data->meta_description);
        \Meta::set('image', asset($data->image));
        return view('frontend.site.page')->with('data', $data);
    }

    public function childpage($slug)
    {
        $data = \App\Models\Admin\ChildPages::findBySlug($slug);
        \Meta::set('title', $data->title);
        \Meta::set('description', $data->meta_description);
        \Meta::set('image', asset($data->image));
        return view('frontend.site.childpage')->with('data', $data);
    }

    public function jobs()
    {
        $jobs = \App\Models\Admin\Job::orderBy('id', 'DESC')->get();

        return view('frontend.site.jobs')->with('jobs', $jobs);
    }

    public function jobsDetail($slug)
    {
        $job = \App\Models\Admin\Job::findBySlug($slug);
        \Meta::set('title', $job->title);
        return view('frontend.site.jobsDetail')->with('job', $job);
    }


    public function stories()
    {
        $stories = Story::all();
        return view('frontend.site.stories')->with('stories', $stories);
    }

    public function news($slug)
    {

        $page = \App\Models\Admin\News::findBySlug($slug);
        \Meta::set('title', $page->title);
        \Meta::set('description', $page->meta_description);

        return view('frontend.site.news')->with('page', $page);

    }


    public function emailSend(Request $request)
    {

        $data = $request->all();
        $file = $request->file('file');
        $sent = \Mail::send('emails.email', ['data' => $data, 'file' => $file], function ($m) use ($data, $file) {
            $m->to("info@paksnaps.com", "paksnaps")->subject('New inquiry recieved');

        });
        alert()->success('Your inquiry has successfully submitted', 'Thanks')->persistent('close');
        return redirect('/');

    }
    public function sendCV(Request $request)
    {

        $data = $request->all();
        $file = $request->file('file');
        $sent = \Mail::send('emails.email', ['data' => $data, 'file' => $file], function ($m) use ($data, $file) {
            $m->to("mrsmanpower836@gmail.com", "MRS")->subject('New inquiry recieved');
            $m->attach($file->getRealPath(),
                [
                    'as' => $file->getClientOriginalName(),
                    'mime' => $file->getMimeType()
                ]
            );
        });
        alert()->success('Your inquiry has successfully submitted', 'Thanks')->persistent('close');
        return redirect('/');

    }

    public function galleryCategory($category)
    {

        $images = Gallery::where('category', '=', $category)->get();
        $page = \App\Models\Admin\Page::whereName('gallery')->first();
        \Meta::set('title', $page->title);
        \Meta::set('description', $page->meta_description);
        \Meta::set('image', asset($page->image));

        return view('frontend.site.gallery_detail')
            ->with('page', $page)
            ->with('images', $images);

    }

    public function categories($id)
    {

        $category = Category::find($id);

//        return $category->books;
        \Meta::set('title', $category->title);
        \Meta::set('description', $category->meta_description);
        \Meta::set('image', asset($category->image));


        return view('frontend.site.books')->with('category', $category);

    }

    public function book($id)
    {
        $book = Book::find($id);
        \Meta::set('title', $book->title);
        \Meta::set('description', $book->meta_description);
        \Meta::set('image', asset($book->image));
        return view('frontend.site.book_detail')->with('book', $book);

    }


}
