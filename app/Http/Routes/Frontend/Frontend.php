<?php

/**
 * Frontend Controllers
 */




Route::get('test',function (){

//    return \App\Models\Admin\Book::find(1)->subCategory;
    return \App\Models\Admin\SubCategory::find(1)->books;
});

Route::get('/', 'FrontendController@index')->name('frontend.index');
Route::get('/profile', 'FrontendController@profile')->name('frontend.profile');
Route::get('/service', 'FrontendController@service')->name('frontend.service');
Route::get('/privacy', 'FrontendController@privacy')->name('frontend.privacy');
Route::get('/term', 'FrontendController@term')->name('frontend.term');
Route::get('/contact', 'FrontendController@contact')->name('frontend.contact');


Route::get('/products/{id}', ['as' => 'products', 'uses' => 'FrontendController@products']);
Route::get('/air-compressors/{slug}', ['as' => 'airCompressors', 'uses' => 'FrontendController@airCompressors']);
Route::get('/air-accessories/drayers/', ['as' => 'airAccessories.drayers', 'uses' => 'FrontendController@airAccessoriesDrayers']);
Route::get('/air-accessories/filters/', ['as' => 'airAccessories.filters', 'uses' => 'FrontendController@airAccessoriesFilters']);
Route::get('/air-accessories/{slug}', ['as' => 'airAccessories', 'uses' => 'FrontendController@airAccessories']);


Route::get('/category/{id}', ['as' => 'category', 'uses' => 'FrontendController@categories']);
Route::get('/book/{id}', ['as' => 'book', 'uses' => 'FrontendController@book']);
//Route::get('/category-type/{id}', ['as' => 'category.type', 'uses' => 'FrontendController@categoryType']);
//Route::get('/category-accessories/filters', ['as' => 'category.type', 'uses' => 'FrontendController@categoryAccessoriesFilter']);
//Route::get('/category-accessories/dryers', ['as' => 'category.type', 'uses' => 'FrontendController@categoryAccessoriesDryers']);
Route::get('/gallery/{category}', ['as' => 'gallery.category', 'uses' => 'FrontendController@galleryCategory']);



Route::post('/email-send', ['as' => 'email-send', 'uses' => 'FrontendController@emailSend']);

Route::post('/send-cv', ['as' => 'email-send', 'uses' => 'FrontendController@sendCV']);




Route::get('news/{slug}', ['as' => 'news', 'uses' => 'FrontendController@news']);
Route::get('files/{id}', ['as' => 'files', 'uses' => 'FrontendController@downloadBooks']);

Route::get('jobs', ['as' => 'jobs', 'uses' => 'FrontendController@jobs']);
Route::get('stories', ['as' => 'stories', 'uses' => 'FrontendController@stories']);
Route::get('jobs/{slug}', ['as' => 'jobsDetail', 'uses' => 'FrontendController@jobsDetail']);
Route::get('childpage/{slug}', ['as' => 'childpage', 'uses' => 'FrontendController@childpage']);



Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User'], function () {
        Route::get('dashboard', 'DashboardController@index')->name('frontend.user.dashboard');
        Route::get('profile/edit', 'ProfileController@edit')->name('frontend.user.profile.edit');
        Route::patch('profile/update', 'ProfileController@update')->name('frontend.user.profile.update');
    });
});