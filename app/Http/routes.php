<?php

Route::group(['middleware' => 'web'], function () {
    /**
     * Switch between the included languages
     */
    Route::group(['namespace' => 'Language'], function () {
        require(__DIR__ . '/Routes/Language/Language.php');
    });

    /**
     * Frontend Routes
     * Namespaces indicate folder structure
     */
//    Route::resource('posts', 'PostController');

    Route::group(['namespace' => 'Frontend'], function () {
        require(__DIR__ . '/Routes/Frontend/Frontend.php');
        require(__DIR__ . '/Routes/Frontend/Access.php');


    });
});


/**
 * Backend Routes
 * Namespaces indicate folder structure
 * Admin middleware groups web, auth, and routeNeedsPermission
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'middleware' => 'admin'], function () {
    /**
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     */
    require(__DIR__ . '/Routes/Backend/Dashboard.php');
    require(__DIR__ . '/Routes/Backend/Access.php');
    require(__DIR__ . '/Routes/Backend/LogViewer.php');
});


/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/


Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {
    Route::group(['prefix' => 'v1'], function () {
        require config('infyom.laravel_generator.path.api_routes');
    });
});


Route::group(['middleware' => 'admin'], function () {
    

    Route::get('admin/banners', ['as' => 'admin.banners.index', 'uses' => 'Admin\BannerController@index']);
    Route::post('admin/banners', ['as' => 'admin.banners.store', 'uses' => 'Admin\BannerController@store']);
    Route::get('admin/banners/create', ['as' => 'admin.banners.create', 'uses' => 'Admin\BannerController@create']);
    Route::put('admin/banners/{banners}', ['as' => 'admin.banners.update', 'uses' => 'Admin\BannerController@update']);
    Route::patch('admin/banners/{banners}', ['as' => 'admin.banners.update', 'uses' => 'Admin\BannerController@update']);
    Route::delete('admin/banners/{banners}', ['as' => 'admin.banners.destroy', 'uses' => 'Admin\BannerController@destroy']);
    Route::get('admin/banners/{banners}', ['as' => 'admin.banners.show', 'uses' => 'Admin\BannerController@show']);
    Route::get('admin/banners/{banners}/edit', ['as' => 'admin.banners.edit', 'uses' => 'Admin\BannerController@edit']);


    Route::get('admin/pages', ['as' => 'admin.pages.index', 'uses' => 'Admin\PageController@index']);
    Route::post('admin/pages', ['as' => 'admin.pages.store', 'uses' => 'Admin\PageController@store']);
    Route::get('admin/pages/create', ['as' => 'admin.pages.create', 'uses' => 'Admin\PageController@create']);
    Route::put('admin/pages/{pages}', ['as' => 'admin.pages.update', 'uses' => 'Admin\PageController@update']);
    Route::patch('admin/pages/{pages}', ['as' => 'admin.pages.update', 'uses' => 'Admin\PageController@update']);
    Route::delete('admin/pages/{pages}', ['as' => 'admin.pages.destroy', 'uses' => 'Admin\PageController@destroy']);
    Route::get('admin/pages/{pages}', ['as' => 'admin.pages.show', 'uses' => 'Admin\PageController@show']);
    Route::get('admin/pages/{pages}/edit', ['as' => 'admin.pages.edit', 'uses' => 'Admin\PageController@edit']);


    Route::get('admin/locations', ['as'=> 'admin.locations.index', 'uses' => 'Admin\LocationController@index']);
    Route::post('admin/locations', ['as'=> 'admin.locations.store', 'uses' => 'Admin\LocationController@store']);
    Route::get('admin/locations/create', ['as'=> 'admin.locations.create', 'uses' => 'Admin\LocationController@create']);
    Route::put('admin/locations/{locations}', ['as'=> 'admin.locations.update', 'uses' => 'Admin\LocationController@update']);
    Route::patch('admin/locations/{locations}', ['as'=> 'admin.locations.update', 'uses' => 'Admin\LocationController@update']);
    Route::delete('admin/locations/{locations}', ['as'=> 'admin.locations.destroy', 'uses' => 'Admin\LocationController@destroy']);
    Route::get('admin/locations/{locations}', ['as'=> 'admin.locations.show', 'uses' => 'Admin\LocationController@show']);
    Route::get('admin/locations/{locations}/edit', ['as'=> 'admin.locations.edit', 'uses' => 'Admin\LocationController@edit']);



    Route::get('admin/news', ['as'=> 'admin.news.index', 'uses' => 'Admin\NewsController@index']);
    Route::post('admin/news', ['as'=> 'admin.news.store', 'uses' => 'Admin\NewsController@store']);
    Route::get('admin/news/create', ['as'=> 'admin.news.create', 'uses' => 'Admin\NewsController@create']);
    Route::put('admin/news/{news}', ['as'=> 'admin.news.update', 'uses' => 'Admin\NewsController@update']);
    Route::patch('admin/news/{news}', ['as'=> 'admin.news.update', 'uses' => 'Admin\NewsController@update']);
    Route::delete('admin/news/{news}', ['as'=> 'admin.news.destroy', 'uses' => 'Admin\NewsController@destroy']);
    Route::get('admin/news/{news}', ['as'=> 'admin.news.show', 'uses' => 'Admin\NewsController@show']);
    Route::get('admin/news/{news}/edit', ['as'=> 'admin.news.edit', 'uses' => 'Admin\NewsController@edit']);

    

    Route::get('admin/maps', ['as'=> 'admin.maps.index', 'uses' => 'Admin\MapController@index']);
    Route::post('admin/maps', ['as'=> 'admin.maps.store', 'uses' => 'Admin\MapController@store']);
    Route::get('admin/maps/create', ['as'=> 'admin.maps.create', 'uses' => 'Admin\MapController@create']);
    Route::put('admin/maps/{maps}', ['as'=> 'admin.maps.update', 'uses' => 'Admin\MapController@update']);
    Route::patch('admin/maps/{maps}', ['as'=> 'admin.maps.update', 'uses' => 'Admin\MapController@update']);
    Route::delete('admin/maps/{maps}', ['as'=> 'admin.maps.destroy', 'uses' => 'Admin\MapController@destroy']);
    Route::get('admin/maps/{maps}', ['as'=> 'admin.maps.show', 'uses' => 'Admin\MapController@show']);
    Route::get('admin/maps/{maps}/edit', ['as'=> 'admin.maps.edit', 'uses' => 'Admin\MapController@edit']);
    

    Route::get('admin/galleries', ['as'=> 'admin.galleries.index', 'uses' => 'Admin\GalleryController@index']);
    Route::post('admin/galleries', ['as'=> 'admin.galleries.store', 'uses' => 'Admin\GalleryController@store']);
    Route::get('admin/galleries/create', ['as'=> 'admin.galleries.create', 'uses' => 'Admin\GalleryController@create']);
    Route::put('admin/galleries/{galleries}', ['as'=> 'admin.galleries.update', 'uses' => 'Admin\GalleryController@update']);
    Route::patch('admin/galleries/{galleries}', ['as'=> 'admin.galleries.update', 'uses' => 'Admin\GalleryController@update']);
    Route::delete('admin/galleries/{galleries}', ['as'=> 'admin.galleries.destroy', 'uses' => 'Admin\GalleryController@destroy']);
    Route::get('admin/galleries/{galleries}', ['as'=> 'admin.galleries.show', 'uses' => 'Admin\GalleryController@show']);
    Route::get('admin/galleries/{galleries}/edit', ['as'=> 'admin.galleries.edit', 'uses' => 'Admin\GalleryController@edit']);

    

    Route::get('admin/contacts', ['as'=> 'admin.contacts.index', 'uses' => 'Admin\ContactController@index']);
    Route::post('admin/contacts', ['as'=> 'admin.contacts.store', 'uses' => 'Admin\ContactController@store']);
    Route::get('admin/contacts/create', ['as'=> 'admin.contacts.create', 'uses' => 'Admin\ContactController@create']);
    Route::put('admin/contacts/{contacts}', ['as'=> 'admin.contacts.update', 'uses' => 'Admin\ContactController@update']);
    Route::patch('admin/contacts/{contacts}', ['as'=> 'admin.contacts.update', 'uses' => 'Admin\ContactController@update']);
    Route::delete('admin/contacts/{contacts}', ['as'=> 'admin.contacts.destroy', 'uses' => 'Admin\ContactController@destroy']);
    Route::get('admin/contacts/{contacts}', ['as'=> 'admin.contacts.show', 'uses' => 'Admin\ContactController@show']);
    Route::get('admin/contacts/{contacts}/edit', ['as'=> 'admin.contacts.edit', 'uses' => 'Admin\ContactController@edit']);




    Route::get('admin/subCategories', ['as'=> 'admin.subCategories.index', 'uses' => 'Admin\SubCategoryController@index']);
    Route::post('admin/subCategories', ['as'=> 'admin.subCategories.store', 'uses' => 'Admin\SubCategoryController@store']);
    Route::get('admin/subCategories/create', ['as'=> 'admin.subCategories.create', 'uses' => 'Admin\SubCategoryController@create']);
    Route::put('admin/subCategories/{subCategories}', ['as'=> 'admin.subCategories.update', 'uses' => 'Admin\SubCategoryController@update']);
    Route::patch('admin/subCategories/{subCategories}', ['as'=> 'admin.subCategories.update', 'uses' => 'Admin\SubCategoryController@update']);
    Route::delete('admin/subCategories/{subCategories}', ['as'=> 'admin.subCategories.destroy', 'uses' => 'Admin\SubCategoryController@destroy']);
    Route::get('admin/subCategories/{subCategories}', ['as'=> 'admin.subCategories.show', 'uses' => 'Admin\SubCategoryController@show']);
    Route::get('admin/subCategories/{subCategories}/edit', ['as'=> 'admin.subCategories.edit', 'uses' => 'Admin\SubCategoryController@edit']);



    Route::get('admin/books', ['as'=> 'admin.books.index', 'uses' => 'Admin\BookController@index']);
    Route::post('admin/books', ['as'=> 'admin.books.store', 'uses' => 'Admin\BookController@store']);
    Route::get('admin/books/create', ['as'=> 'admin.books.create', 'uses' => 'Admin\BookController@create']);
    Route::put('admin/books/{books}', ['as'=> 'admin.books.update', 'uses' => 'Admin\BookController@update']);
    Route::patch('admin/books/{books}', ['as'=> 'admin.books.update', 'uses' => 'Admin\BookController@update']);
    Route::delete('admin/books/{books}', ['as'=> 'admin.books.destroy', 'uses' => 'Admin\BookController@destroy']);
    Route::get('admin/books/{books}', ['as'=> 'admin.books.show', 'uses' => 'Admin\BookController@show']);
    Route::get('admin/books/{books}/edit', ['as'=> 'admin.books.edit', 'uses' => 'Admin\BookController@edit']);


    Route::get('admin/brands', ['as'=> 'admin.brands.index', 'uses' => 'Admin\BrandController@index']);
    Route::post('admin/brands', ['as'=> 'admin.brands.store', 'uses' => 'Admin\BrandController@store']);
    Route::get('admin/brands/create', ['as'=> 'admin.brands.create', 'uses' => 'Admin\BrandController@create']);
    Route::put('admin/brands/{brands}', ['as'=> 'admin.brands.update', 'uses' => 'Admin\BrandController@update']);
    Route::patch('admin/brands/{brands}', ['as'=> 'admin.brands.update', 'uses' => 'Admin\BrandController@update']);
    Route::delete('admin/brands/{brands}', ['as'=> 'admin.brands.destroy', 'uses' => 'Admin\BrandController@destroy']);
    Route::get('admin/brands/{brands}', ['as'=> 'admin.brands.show', 'uses' => 'Admin\BrandController@show']);
    Route::get('admin/brands/{brands}/edit', ['as'=> 'admin.brands.edit', 'uses' => 'Admin\BrandController@edit']);


    Route::get('admin/reviews', ['as'=> 'admin.reviews.index', 'uses' => 'Admin\ReviewController@index']);
    Route::post('admin/reviews', ['as'=> 'admin.reviews.store', 'uses' => 'Admin\ReviewController@store']);
    Route::get('admin/reviews/create', ['as'=> 'admin.reviews.create', 'uses' => 'Admin\ReviewController@create']);
    Route::put('admin/reviews/{reviews}', ['as'=> 'admin.reviews.update', 'uses' => 'Admin\ReviewController@update']);
    Route::patch('admin/reviews/{reviews}', ['as'=> 'admin.reviews.update', 'uses' => 'Admin\ReviewController@update']);
    Route::delete('admin/reviews/{reviews}', ['as'=> 'admin.reviews.destroy', 'uses' => 'Admin\ReviewController@destroy']);
    Route::get('admin/reviews/{reviews}', ['as'=> 'admin.reviews.show', 'uses' => 'Admin\ReviewController@show']);
    Route::get('admin/reviews/{reviews}/edit', ['as'=> 'admin.reviews.edit', 'uses' => 'Admin\ReviewController@edit']);



    Route::get('admin/categories', ['as'=> 'admin.categories.index', 'uses' => 'Admin\CategoryController@index']);
    Route::post('admin/categories', ['as'=> 'admin.categories.store', 'uses' => 'Admin\CategoryController@store']);
    Route::get('admin/categories/create', ['as'=> 'admin.categories.create', 'uses' => 'Admin\CategoryController@create']);
    Route::put('admin/categories/{categories}', ['as'=> 'admin.categories.update', 'uses' => 'Admin\CategoryController@update']);
    Route::patch('admin/categories/{categories}', ['as'=> 'admin.categories.update', 'uses' => 'Admin\CategoryController@update']);
    Route::delete('admin/categories/{categories}', ['as'=> 'admin.categories.destroy', 'uses' => 'Admin\CategoryController@destroy']);
    Route::get('admin/categories/{categories}', ['as'=> 'admin.categories.show', 'uses' => 'Admin\CategoryController@show']);
    Route::get('admin/categories/{categories}/edit', ['as'=> 'admin.categories.edit', 'uses' => 'Admin\CategoryController@edit']);




    Route::get('admin/downloadBooks', ['as'=> 'admin.downloadBooks.index', 'uses' => 'Admin\DownloadBookController@index']);
    Route::post('admin/downloadBooks', ['as'=> 'admin.downloadBooks.store', 'uses' => 'Admin\DownloadBookController@store']);
    Route::get('admin/downloadBooks/create', ['as'=> 'admin.downloadBooks.create', 'uses' => 'Admin\DownloadBookController@create']);
    Route::put('admin/downloadBooks/{downloadBooks}', ['as'=> 'admin.downloadBooks.update', 'uses' => 'Admin\DownloadBookController@update']);
    Route::patch('admin/downloadBooks/{downloadBooks}', ['as'=> 'admin.downloadBooks.update', 'uses' => 'Admin\DownloadBookController@update']);
    Route::delete('admin/downloadBooks/{downloadBooks}', ['as'=> 'admin.downloadBooks.destroy', 'uses' => 'Admin\DownloadBookController@destroy']);
    Route::get('admin/downloadBooks/{downloadBooks}', ['as'=> 'admin.downloadBooks.show', 'uses' => 'Admin\DownloadBookController@show']);
    Route::get('admin/downloadBooks/{downloadBooks}/edit', ['as'=> 'admin.downloadBooks.edit', 'uses' => 'Admin\DownloadBookController@edit']);




    Route::get('admin/downloadPapers', ['as'=> 'admin.downloadPapers.index', 'uses' => 'Admin\DownloadPaperController@index']);
    Route::post('admin/downloadPapers', ['as'=> 'admin.downloadPapers.store', 'uses' => 'Admin\DownloadPaperController@store']);
    Route::get('admin/downloadPapers/create', ['as'=> 'admin.downloadPapers.create', 'uses' => 'Admin\DownloadPaperController@create']);
    Route::put('admin/downloadPapers/{downloadPapers}', ['as'=> 'admin.downloadPapers.update', 'uses' => 'Admin\DownloadPaperController@update']);
    Route::patch('admin/downloadPapers/{downloadPapers}', ['as'=> 'admin.downloadPapers.update', 'uses' => 'Admin\DownloadPaperController@update']);
    Route::delete('admin/downloadPapers/{downloadPapers}', ['as'=> 'admin.downloadPapers.destroy', 'uses' => 'Admin\DownloadPaperController@destroy']);
    Route::get('admin/downloadPapers/{downloadPapers}', ['as'=> 'admin.downloadPapers.show', 'uses' => 'Admin\DownloadPaperController@show']);
    Route::get('admin/downloadPapers/{downloadPapers}/edit', ['as'=> 'admin.downloadPapers.edit', 'uses' => 'Admin\DownloadPaperController@edit']);


    Route::get('admin/projects', ['as'=> 'admin.projects.index', 'uses' => 'Admin\ProjectController@index']);
    Route::post('admin/projects', ['as'=> 'admin.projects.store', 'uses' => 'Admin\ProjectController@store']);
    Route::get('admin/projects/create', ['as'=> 'admin.projects.create', 'uses' => 'Admin\ProjectController@create']);
    Route::put('admin/projects/{projects}', ['as'=> 'admin.projects.update', 'uses' => 'Admin\ProjectController@update']);
    Route::patch('admin/projects/{projects}', ['as'=> 'admin.projects.update', 'uses' => 'Admin\ProjectController@update']);
    Route::delete('admin/projects/{projects}', ['as'=> 'admin.projects.destroy', 'uses' => 'Admin\ProjectController@destroy']);
    Route::get('admin/projects/{projects}', ['as'=> 'admin.projects.show', 'uses' => 'Admin\ProjectController@show']);
    Route::get('admin/projects/{projects}/edit', ['as'=> 'admin.projects.edit', 'uses' => 'Admin\ProjectController@edit']);



    Route::get('admin/clients', ['as'=> 'admin.clients.index', 'uses' => 'Admin\ClientController@index']);
    Route::post('admin/clients', ['as'=> 'admin.clients.store', 'uses' => 'Admin\ClientController@store']);
    Route::get('admin/clients/create', ['as'=> 'admin.clients.create', 'uses' => 'Admin\ClientController@create']);
    Route::put('admin/clients/{clients}', ['as'=> 'admin.clients.update', 'uses' => 'Admin\ClientController@update']);
    Route::patch('admin/clients/{clients}', ['as'=> 'admin.clients.update', 'uses' => 'Admin\ClientController@update']);
    Route::delete('admin/clients/{clients}', ['as'=> 'admin.clients.destroy', 'uses' => 'Admin\ClientController@destroy']);
    Route::get('admin/clients/{clients}', ['as'=> 'admin.clients.show', 'uses' => 'Admin\ClientController@show']);
    Route::get('admin/clients/{clients}/edit', ['as'=> 'admin.clients.edit', 'uses' => 'Admin\ClientController@edit']);


    Route::get('admin/teams', ['as'=> 'admin.teams.index', 'uses' => 'Admin\TeamController@index']);
    Route::post('admin/teams', ['as'=> 'admin.teams.store', 'uses' => 'Admin\TeamController@store']);
    Route::get('admin/teams/create', ['as'=> 'admin.teams.create', 'uses' => 'Admin\TeamController@create']);
    Route::put('admin/teams/{teams}', ['as'=> 'admin.teams.update', 'uses' => 'Admin\TeamController@update']);
    Route::patch('admin/teams/{teams}', ['as'=> 'admin.teams.update', 'uses' => 'Admin\TeamController@update']);
    Route::delete('admin/teams/{teams}', ['as'=> 'admin.teams.destroy', 'uses' => 'Admin\TeamController@destroy']);
    Route::get('admin/teams/{teams}', ['as'=> 'admin.teams.show', 'uses' => 'Admin\TeamController@show']);
    Route::get('admin/teams/{teams}/edit', ['as'=> 'admin.teams.edit', 'uses' => 'Admin\TeamController@edit']);


    Route::get('admin/projectDetails', ['as'=> 'admin.projectDetails.index', 'uses' => 'Admin\ProjectDetailController@index']);
    Route::post('admin/projectDetails', ['as'=> 'admin.projectDetails.store', 'uses' => 'Admin\ProjectDetailController@store']);
    Route::get('admin/projectDetails/create', ['as'=> 'admin.projectDetails.create', 'uses' => 'Admin\ProjectDetailController@create']);
    Route::put('admin/projectDetails/{projectDetails}', ['as'=> 'admin.projectDetails.update', 'uses' => 'Admin\ProjectDetailController@update']);
    Route::patch('admin/projectDetails/{projectDetails}', ['as'=> 'admin.projectDetails.update', 'uses' => 'Admin\ProjectDetailController@update']);
    Route::delete('admin/projectDetails/{projectDetails}', ['as'=> 'admin.projectDetails.destroy', 'uses' => 'Admin\ProjectDetailController@destroy']);
    Route::get('admin/projectDetails/{projectDetails}', ['as'=> 'admin.projectDetails.show', 'uses' => 'Admin\ProjectDetailController@show']);
    Route::get('admin/projectDetails/{projectDetails}/edit', ['as'=> 'admin.projectDetails.edit', 'uses' => 'Admin\ProjectDetailController@edit']);

    Route::get('admin/certifications', ['as'=> 'admin.certifications.index', 'uses' => 'Admin\CertificationController@index']);
    Route::post('admin/certifications', ['as'=> 'admin.certifications.store', 'uses' => 'Admin\CertificationController@store']);
    Route::get('admin/certifications/create', ['as'=> 'admin.certifications.create', 'uses' => 'Admin\CertificationController@create']);
    Route::put('admin/certifications/{certifications}', ['as'=> 'admin.certifications.update', 'uses' => 'Admin\CertificationController@update']);
    Route::patch('admin/certifications/{certifications}', ['as'=> 'admin.certifications.update', 'uses' => 'Admin\CertificationController@update']);
    Route::delete('admin/certifications/{certifications}', ['as'=> 'admin.certifications.destroy', 'uses' => 'Admin\CertificationController@destroy']);
    Route::get('admin/certifications/{certifications}', ['as'=> 'admin.certifications.show', 'uses' => 'Admin\CertificationController@show']);
    Route::get('admin/certifications/{certifications}/edit', ['as'=> 'admin.certifications.edit', 'uses' => 'Admin\CertificationController@edit']);

    Route::get('admin/jobs', ['as'=> 'admin.jobs.index', 'uses' => 'Admin\JobController@index']);
    Route::post('admin/jobs', ['as'=> 'admin.jobs.store', 'uses' => 'Admin\JobController@store']);
    Route::get('admin/jobs/create', ['as'=> 'admin.jobs.create', 'uses' => 'Admin\JobController@create']);
    Route::put('admin/jobs/{jobs}', ['as'=> 'admin.jobs.update', 'uses' => 'Admin\JobController@update']);
    Route::patch('admin/jobs/{jobs}', ['as'=> 'admin.jobs.update', 'uses' => 'Admin\JobController@update']);
    Route::delete('admin/jobs/{jobs}', ['as'=> 'admin.jobs.destroy', 'uses' => 'Admin\JobController@destroy']);
    Route::get('admin/jobs/{jobs}', ['as'=> 'admin.jobs.show', 'uses' => 'Admin\JobController@show']);
    Route::get('admin/jobs/{jobs}/edit', ['as'=> 'admin.jobs.edit', 'uses' => 'Admin\JobController@edit']);



    Route::get('admin/childPages', ['as'=> 'admin.childPages.index', 'uses' => 'Admin\ChildPagesController@index']);
    Route::post('admin/childPages', ['as'=> 'admin.childPages.store', 'uses' => 'Admin\ChildPagesController@store']);
    Route::get('admin/childPages/create', ['as'=> 'admin.childPages.create', 'uses' => 'Admin\ChildPagesController@create']);
    Route::put('admin/childPages/{childPages}', ['as'=> 'admin.childPages.update', 'uses' => 'Admin\ChildPagesController@update']);
    Route::patch('admin/childPages/{childPages}', ['as'=> 'admin.childPages.update', 'uses' => 'Admin\ChildPagesController@update']);
    Route::delete('admin/childPages/{childPages}', ['as'=> 'admin.childPages.destroy', 'uses' => 'Admin\ChildPagesController@destroy']);
    Route::get('admin/childPages/{childPages}', ['as'=> 'admin.childPages.show', 'uses' => 'Admin\ChildPagesController@show']);
    Route::get('admin/childPages/{childPages}/edit', ['as'=> 'admin.childPages.edit', 'uses' => 'Admin\ChildPagesController@edit']);


    Route::get('admin/quickLinks', ['as'=> 'admin.quickLinks.index', 'uses' => 'Admin\QuickLinkController@index']);
    Route::post('admin/quickLinks', ['as'=> 'admin.quickLinks.store', 'uses' => 'Admin\QuickLinkController@store']);
    Route::get('admin/quickLinks/create', ['as'=> 'admin.quickLinks.create', 'uses' => 'Admin\QuickLinkController@create']);
    Route::put('admin/quickLinks/{quickLinks}', ['as'=> 'admin.quickLinks.update', 'uses' => 'Admin\QuickLinkController@update']);
    Route::patch('admin/quickLinks/{quickLinks}', ['as'=> 'admin.quickLinks.update', 'uses' => 'Admin\QuickLinkController@update']);
    Route::delete('admin/quickLinks/{quickLinks}', ['as'=> 'admin.quickLinks.destroy', 'uses' => 'Admin\QuickLinkController@destroy']);
    Route::get('admin/quickLinks/{quickLinks}', ['as'=> 'admin.quickLinks.show', 'uses' => 'Admin\QuickLinkController@show']);
    Route::get('admin/quickLinks/{quickLinks}/edit', ['as'=> 'admin.quickLinks.edit', 'uses' => 'Admin\QuickLinkController@edit']);




    Route::get('admin/stories', ['as'=> 'admin.stories.index', 'uses' => 'Admin\StoryController@index']);
    Route::post('admin/stories', ['as'=> 'admin.stories.store', 'uses' => 'Admin\StoryController@store']);
    Route::get('admin/stories/create', ['as'=> 'admin.stories.create', 'uses' => 'Admin\StoryController@create']);
    Route::put('admin/stories/{stories}', ['as'=> 'admin.stories.update', 'uses' => 'Admin\StoryController@update']);
    Route::patch('admin/stories/{stories}', ['as'=> 'admin.stories.update', 'uses' => 'Admin\StoryController@update']);
    Route::delete('admin/stories/{stories}', ['as'=> 'admin.stories.destroy', 'uses' => 'Admin\StoryController@destroy']);
    Route::get('admin/stories/{stories}', ['as'=> 'admin.stories.show', 'uses' => 'Admin\StoryController@show']);
    Route::get('admin/stories/{stories}/edit', ['as'=> 'admin.stories.edit', 'uses' => 'Admin\StoryController@edit']);




    Route::get('admin/userImages', ['as'=> 'admin.userImages.index', 'uses' => 'Admin\UserImageController@index']);
    Route::post('admin/userImages', ['as'=> 'admin.userImages.store', 'uses' => 'Admin\UserImageController@store']);
    Route::get('admin/userImages/create', ['as'=> 'admin.userImages.create', 'uses' => 'Admin\UserImageController@create']);
    Route::put('admin/userImages/{userImages}', ['as'=> 'admin.userImages.update', 'uses' => 'Admin\UserImageController@update']);
    Route::patch('admin/userImages/{userImages}', ['as'=> 'admin.userImages.update', 'uses' => 'Admin\UserImageController@update']);
    Route::delete('admin/userImages/{userImages}', ['as'=> 'admin.userImages.destroy', 'uses' => 'Admin\UserImageController@destroy']);
    Route::get('admin/userImages/{userImages}', ['as'=> 'admin.userImages.show', 'uses' => 'Admin\UserImageController@show']);
    Route::get('admin/userImages/{userImages}/edit', ['as'=> 'admin.userImages.edit', 'uses' => 'Admin\UserImageController@edit']);

});






$middleware = array_merge(\Config::get('lfm.middlewares'), ['\Unisharp\Laravelfilemanager\middleware\MultiUser']);
$prefix = \Config::get('lfm.prefix', 'laravel-filemanager');
$as = 'unisharp.lfm.';
$namespace = '\Unisharp\Laravelfilemanager\controllers';

// make sure authenticated
Route::group(compact('middleware', 'prefix', 'as', 'namespace'), function () {

    // Show LFM
    Route::get('/', [
        'uses' => 'LfmController@show',
        'as' => 'show'
    ]);

    // upload
    Route::any('/upload', [
        'uses' => 'UploadController@upload',
        'as' => 'upload'
    ]);

    // list images & files
    Route::get('/jsonitems', [
        'uses' => 'ItemsController@getItems',
        'as' => 'getItems'
    ]);

    // folders
    Route::get('/newfolder', [
        'uses' => 'FolderController@getAddfolder',
        'as' => 'getAddfolder'
    ]);
    Route::get('/deletefolder', [
        'uses' => 'FolderController@getDeletefolder',
        'as' => 'getDeletefolder'
    ]);
    Route::get('/folders', [
        'uses' => 'FolderController@getFolders',
        'as' => 'getFolders'
    ]);

    // crop
    Route::get('/crop', [
        'uses' => 'CropController@getCrop',
        'as' => 'getCrop'
    ]);
    Route::get('/cropimage', [
        'uses' => 'CropController@getCropimage',
        'as' => 'getCropimage'
    ]);

    // rename
    Route::get('/rename', [
        'uses' => 'RenameController@getRename',
        'as' => 'getRename'
    ]);

    // scale/resize
    Route::get('/resize', [
        'uses' => 'ResizeController@getResize',
        'as' => 'getResize'
    ]);
    Route::get('/doresize', [
        'uses' => 'ResizeController@performResize',
        'as' => 'performResize'
    ]);

    // download
    Route::get('/download', [
        'uses' => 'DownloadController@getDownload',
        'as' => 'getDownload'
    ]);

    // delete
    Route::get('/delete', [
        'uses' => 'DeleteController@getDelete',
        'as' => 'getDelete'
    ]);

    Route::get('/demo', function () {
        return view('laravel-filemanager::demo');
    });
});









Route::group(['prefix' => 'niceartisan'], function () {
    Route::get('/{option?}', '\Bestmomo\NiceArtisan\Http\Controllers\NiceArtisanController@show');
    Route::post('item/{class}', '\Bestmomo\NiceArtisan\Http\Controllers\NiceArtisanController@command');
});

Route::get('/{url}', ['as' => 'page', 'uses' => 'Frontend\FrontendController@page']);


Route::group(['prefix' => 'niceartisan'], function () {
    Route::get('/{option?}', '\Bestmomo\NiceArtisan\Http\Controllers\NiceArtisanController@show');
    Route::post('item/{class}', '\Bestmomo\NiceArtisan\Http\Controllers\NiceArtisanController@command');
});


//Route::auth();
//
//Route::get('/home', 'HomeController@index');









