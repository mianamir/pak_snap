<?php
namespace App\Lib\PDF;

use  FPDI;

class PDF extends FPDI
{

    function Header()
    {
        // Logo

        if (\Auth::check()) {
            $this->Image(\Auth::user()->image == '' ? 'assets/img/logo1-default.png' : \Auth::user()->image, 10, 6, 30);
        } else {
            $this->Image('assets/img/logo1-default.png', 10, 6, 30);
        }

        // Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        // Move to the right
        $this->Cell(80);
        // Title

        if (\Auth::check()) {
            $this->Cell(100, 10, \Auth::user()->name == '' ? 'Lasani Publishers' : \Auth::user()->name, 0, 0, 'C');
        } else {
            $this->Cell(100, 10, 'Lasani Publishers', 0, 0, 'C');
        }


        // Line break
        $this->Ln(20);
    }

// Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }
}