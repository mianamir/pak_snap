<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Banner
 * @package App\Models\Admin
 * @version November 8, 2016, 6:55 pm UTC
 */
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Banner extends Model
{
    use SoftDeletes;
    use SluggableScopeHelpers;


    public $table = 'banners';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'title',
        'detail',
        'image',
        'published'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'title' => 'string',
        'detail' => 'string',
        'image' => 'string',
        'published' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
