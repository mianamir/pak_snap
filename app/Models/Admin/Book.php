<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Book
 * @package App\Models\Admin
 * @version December 26, 2016, 7:37 pm UTC
 */
class Book extends Model
{
    use SoftDeletes;

    public $table = 'books';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'book_code',
        'total_pages',
        'price',
        'author',
        'publishers',
        'title',
        'meta_description',
        'meta_keywords',
        'image',
        'demo_file',
        'pdf_file',
        'published',
        'sub_category_id',
        'slug',
        'url',
        'featured',
        'category_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'book_code' => 'string',
        'total_pages' => 'integer',
        'price' => 'integer',
        'author' => 'string',
        'publishers' => 'string',
        'title' => 'string',
        'meta_description' => 'string',
        'meta_keywords' => 'string',
        'image' => 'string',
        'demo_file' => 'string',
        'pdf_file' => 'string',
        'published' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'book_code' => 'required',
        'total_pages' => 'required',
        'price' => 'required',
        'author' => 'required',
        'publishers' => 'required'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }


}
