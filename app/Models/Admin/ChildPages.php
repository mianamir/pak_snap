<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ChildPages
 * @package App\Models\Admin
 * @version April 25, 2017, 9:50 pm UTC
 */

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
class ChildPages extends Model
{
    use SoftDeletes;
    use SluggableScopeHelpers;

    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug'
            ]
        ];
    }


    public $table = 'child_pages';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'details',
        'title',
        'image',
        'meta_description',
        'slug',
        'heading',
        'page_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'details' => 'string',
        'title' => 'string',
        'image' => 'string',
        'meta_description' => 'string',
        'slug' => 'string',
        'heading' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',

    ];

    
}
