<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DownloadBook
 * @package App\Models\Admin
 * @version March 3, 2017, 7:27 am UTC
 */
class DownloadBook extends Model
{
    use SoftDeletes;

    public $table = 'download_books';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'category',
        'class',
        'subject',
        'demo_file',
        'original_file',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'category' => 'string',
        'class' => 'string',
        'subject' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'category' => 'required',
        'class' => 'required'
    ];

    
}
