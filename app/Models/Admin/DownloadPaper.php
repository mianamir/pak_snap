<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DownloadPaper
 * @package App\Models\Admin
 * @version March 19, 2017, 6:49 am UTC
 */
class DownloadPaper extends Model
{
    use SoftDeletes;

    public $table = 'download_papers';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'category',
        'class',
        'subject',
        'demo_file',
        'original_file',
        'type',
        'chapter_or_term',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'category' => 'string',
        'class' => 'string',
        'subject' => 'string',
        'demo_file' => 'string',
        'original_file' => 'string',
        'type' => 'string',
        'chapter_or_term' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'category' => 'required'
    ];

    
}
