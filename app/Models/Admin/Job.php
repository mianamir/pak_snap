<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

/**
 * Class Job
 * @package App\Models\Admin
 * @version April 21, 2017, 11:49 pm UTC
 */
class Job extends Model
{
    use SoftDeletes;

    use SluggableScopeHelpers;

    use Sluggable;

    public $table = 'jobs';

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug'
            ]
        ];
    }



    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'shor_description',
        'long_description',
        'slug',
        'image',
        'published',
        'featured'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'shor_description' => 'string',
        'long_description' => 'string',
        'slug' => 'string',
        'image' => 'string',
        'published' => 'boolean',
        'featured' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required'
    ];

    
}
