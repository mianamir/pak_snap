<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProjectDetail
 * @package App\Models\Admin
 * @version April 14, 2017, 3:51 pm UTC
 */
class ProjectDetail extends Model
{
    use SoftDeletes;

    public $table = 'project_details';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'category',
        'client',
        'scope_of_work',
        'price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'category' => 'string',
        'client' => 'string',
        'scope_of_work' => 'string',
        'price' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'category' => 'required',
        'client' => 'required',
        'price' => 'required'
    ];

    
}
