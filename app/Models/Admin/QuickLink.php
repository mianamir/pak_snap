<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class QuickLink
 * @package App\Models\Admin
 * @version April 26, 2017, 11:25 pm UTC
 */
class QuickLink extends Model
{
    use SoftDeletes;

    public $table = 'quick_links';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'color',
        'link'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'color' => 'string',
        'link' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'color' => 'required'
    ];

    
}
