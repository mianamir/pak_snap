<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Story
 * @package App\Models\Admin
 * @version April 26, 2017, 11:51 pm UTC
 */
class Story extends Model
{
    use SoftDeletes;

    public $table = 'stories';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'details',
        'image','video'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'details' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'details' => 'required'
    ];

    
}
