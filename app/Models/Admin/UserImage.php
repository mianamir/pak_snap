<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserImage
 * @package App\Models\Admin
 * @version July 4, 2017, 5:19 pm UTC
 */
class UserImage extends Model
{
    use SoftDeletes;

    public $table = 'user_images';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'filename',
        'original_filedir',
        'original_extension',
        'original_width',
        'original_height',
        'dir',
        'original_mime',
        'alt_text',
        'user_id',
        'basename'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'filename' => 'string',
        'original_filedir' => 'string',
        'original_extension' => 'string',
        'original_width' => 'string',
        'original_height' => 'string',
        'dir' => 'string',
        'original_mime' => 'string',
        'alt_text' => 'string',
        'user_id' => 'integer',
        'basename' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
