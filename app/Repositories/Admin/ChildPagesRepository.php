<?php

namespace App\Repositories\Admin;

use App\Models\Admin\ChildPages;
use InfyOm\Generator\Common\BaseRepository;

class ChildPagesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'details',
        'title',
        'image',
        'meta_description',
        'slug',
        'heading',
        'page_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ChildPages::class;
    }
}
