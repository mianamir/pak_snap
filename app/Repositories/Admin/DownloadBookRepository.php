<?php

namespace App\Repositories\Admin;

use App\Models\Admin\DownloadBook;
use InfyOm\Generator\Common\BaseRepository;

class DownloadBookRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'category',
        'class',
        'subject'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DownloadBook::class;
    }
}
