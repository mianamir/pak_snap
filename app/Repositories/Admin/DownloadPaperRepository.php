<?php

namespace App\Repositories\Admin;

use App\Models\Admin\DownloadPaper;
use InfyOm\Generator\Common\BaseRepository;

class DownloadPaperRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cateogry',
        'class',
        'subject',
        'demo_file',
        'original_file',
        'type',
        'chapter_or_term',
        'image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DownloadPaper::class;
    }
}
