<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Job;
use InfyOm\Generator\Common\BaseRepository;

class JobRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'shor_description',
        'long_description',
        'slug',
        'image',
        'published',
        'featured'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Job::class;
    }
}
