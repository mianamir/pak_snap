<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Product;
use InfyOm\Generator\Common\BaseRepository;

class ProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'price',
        'short_description',
        'description',
        'additional_information',
        'technical_specification',
        'help_me_choose',
        'kw_size',
        'dryer',
        'title',
        'meta_description',
        'image',
        'published'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Product::class;
    }
}
