<?php

namespace App\Repositories\Admin;

use App\Models\Admin\ProjectDetail;
use InfyOm\Generator\Common\BaseRepository;

class ProjectDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'category',
        'client',
        'scope_of_work',
        'price'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProjectDetail::class;
    }
}
