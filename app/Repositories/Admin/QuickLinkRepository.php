<?php

namespace App\Repositories\Admin;

use App\Models\Admin\QuickLink;
use InfyOm\Generator\Common\BaseRepository;

class QuickLinkRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'color',
        'link'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return QuickLink::class;
    }
}
