<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Story;
use InfyOm\Generator\Common\BaseRepository;

class StoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'details',
        'image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Story::class;
    }
}
