<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Team;
use InfyOm\Generator\Common\BaseRepository;

class TeamRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'position'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Team::class;
    }
}
