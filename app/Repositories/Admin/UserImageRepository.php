<?php

namespace App\Repositories\Admin;

use App\Models\Admin\UserImage;
use InfyOm\Generator\Common\BaseRepository;

class UserImageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'filename',
        'original_filedir',
        'original_extension',
        'original_width',
        'original_height',
        'dir',
        'original_mime',
        'alt_text',
        'user_id',
        'basename'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserImage::class;
    }
}
