<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBooksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('book_code');
            $table->integer('total_pages');
            $table->integer('price');
            $table->string('author');
            $table->string('publishers');
            $table->string('title');
            $table->text('meta_description');
            $table->text('meta_keywords');
            $table->string('image');
            $table->string('demo_file');
            $table->string('pdf_file');
            $table->boolean('published');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('books');
    }
}
