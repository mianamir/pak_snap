<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDownloadBooksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('download_books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category');
            $table->string('class');
            $table->string('subject');
            $table->string('demo_file');
            $table->string('original_file');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('download_books');
    }
}
