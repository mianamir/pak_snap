<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDownloadPapersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('download_papers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category');
            $table->string('class');
            $table->string('subject');
            $table->string('demo_file');
            $table->string('original_file');
            $table->string('type');
            $table->string('chapter_or_term');
            $table->string('image');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('download_papers');
    }
}
