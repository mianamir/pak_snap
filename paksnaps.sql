-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 03, 2017 at 05:55 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `mrs_consultant`
--

-- --------------------------------------------------------

--
-- Table structure for table `assigned_roles`
--

CREATE TABLE `assigned_roles` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `assigned_roles`
--

INSERT INTO `assigned_roles` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(11, 12, 3);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `name`, `title`, `detail`, `image`, `published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Testing', 'google', 'akusda skdhaskjdhakjsdhakjsdhakdj ad ah', 'uploads/images/qMDHckCMpdteuoVo.jpg', 1, '2016-12-25 04:26:18', '2016-12-27 01:38:21', '2016-12-27 01:38:21'),
(2, 'My Banner', 'ASDASD', 'asdasdoia doadl', 'uploads/images/WDMJQ7xuv29C3xYy.jpg', 1, '2016-12-27 01:40:35', '2017-02-26 04:34:54', '2017-02-26 04:34:54'),
(3, 'Banner', 'Title', 'ASDASDASD', 'uploads/images/oV2n52yZX5Yp4N4l.jpg', 1, '2017-02-26 04:35:39', '2017-04-14 02:39:20', '2017-04-14 02:39:20'),
(4, 'Testing', 'ASDASD', 'asdada sudopiasdljk', 'uploads/images/Hcb3TT4HTMlzuzwt.jpg', 0, '2017-02-26 04:36:04', '2017-04-14 02:39:16', '2017-04-14 02:39:16'),
(5, 'Testing', '', '', 'uploads/images/Zs5RCP5YzVks6x9E.jpg', 0, '2017-04-14 02:40:18', '2017-04-14 02:40:39', '2017-04-14 02:40:39'),
(6, 'Test', '', '', 'uploads/images/H041zDcPy4Ln7jPB.jpg', 0, '2017-04-14 02:41:23', '2017-04-22 20:15:24', '2017-04-22 20:15:24'),
(7, 'Test', 'Test', 'sddsd', 'uploads/images/yum0p5cQkZEi1GWF.jpg', 0, '2017-04-22 20:15:57', '2017-04-22 20:15:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `book_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_pages` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publishers` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `demo_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pdf_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sub_category_id` int(10) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured` tinyint(1) NOT NULL,
  `category_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `name`, `book_code`, `total_pages`, `price`, `author`, `publishers`, `title`, `meta_description`, `meta_keywords`, `image`, `demo_file`, `pdf_file`, `published`, `created_at`, `updated_at`, `deleted_at`, `sub_category_id`, `url`, `slug`, `featured`, `category_id`) VALUES
(1, 'asuydasgdjh', 'auydtkagshjd', 0, 0, 'asdiugksajd', 'aisudgkjhb', 'siaudgkahjdb', 'asdiugjhska,dbmn', 'asidulsgakdj', 'uploads/images/e4eVrT4Cfp1fGSSn.jpg', '', '', 1, '2016-12-26 14:50:00', '2016-12-27 00:17:56', NULL, 1, 'ausydgaisdiuakdjh', '', 0, 1),
(2, 'Test', 'sasdad', 0, 0, 'asdasd', 'asdasd', 'sdasda', 'asd 7ausdaidukgaksjsgdk asgd', 'kausd uakd ajds', '', '', '', 0, '2016-12-26 15:01:26', '2016-12-26 15:01:26', NULL, 1, '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hello', 'uploads/images/4kwNqTnBb2AVLSbp.jpg', '2016-12-27 01:26:29', '2016-12-27 01:32:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `url`, `title`, `meta_keywords`, `meta_description`, `parent_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Adnan', 'Testing', '', '', '', 0, '2017-02-25 14:54:14', '2017-02-25 23:47:02', NULL),
(2, 'Testing', 'asdad', '', '', '', 0, '2017-02-25 14:56:35', '2017-02-25 15:20:04', NULL),
(3, 'Adnan Mumtaz Mayo', 'asdasdasd', '', '', '', 1, '2017-02-25 15:30:02', '2017-02-25 15:30:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `certifications`
--

CREATE TABLE `certifications` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `certifications`
--

INSERT INTO `certifications` (`id`, `name`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 'uploads/images/aiFwjTfk3rYtVEYq.jpg', '2017-04-14 13:22:03', '2017-04-14 13:22:04', NULL),
(2, 'pec', 'uploads/images/HaIug8JY69EKyaN4.jpg', '2017-04-14 13:22:12', '2017-04-14 13:22:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `child_pages`
--

CREATE TABLE `child_pages` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `child_pages`
--

INSERT INTO `child_pages` (`id`, `name`, `details`, `title`, `image`, `meta_description`, `slug`, `heading`, `page_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Test', 'ASDASDAD', 'aSDASDASD', '', 'ASDASD', 'asdasd', 'asdasdasd', 13, '2017-04-25 16:58:58', '2017-04-25 16:59:11', NULL),
(2, 'Tesogn', 'asdasdad', 'asdasda', '', 'asddad', 'adasasd', 'asdad', 2, '2017-04-25 18:15:15', '2017-04-25 18:15:15', NULL),
(3, 'adad', '<p>adsads</p>\r\n', 'adsasd', 'uploads/images/kIoXBvh7wwMcFQQo.jpg', 'adasd', 'asdasd', 'asdasdas', 2, '2017-05-07 20:24:07', '2017-05-07 20:24:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Test', '01.png', '2017-04-14 06:22:05', '2017-04-14 06:23:17', '2017-04-14 06:23:17'),
(2, '', '02.png', '2017-04-14 06:22:15', '2017-04-14 06:23:27', '2017-04-14 06:23:27'),
(3, '', '03.png', '2017-04-14 06:22:23', '2017-04-14 06:23:20', '2017-04-14 06:23:20'),
(4, '', '04.png', '2017-04-14 06:22:31', '2017-04-14 06:23:35', '2017-04-14 06:23:35'),
(5, '', '05.png', '2017-04-14 06:22:39', '2017-04-14 06:23:29', '2017-04-14 06:23:29'),
(6, '', '05.png', '2017-04-14 06:22:48', '2017-04-14 06:23:43', '2017-04-14 06:23:43'),
(7, '', '06.png', '2017-04-14 06:22:57', '2017-04-14 06:23:38', '2017-04-14 06:23:38'),
(8, '', '06.png', '2017-04-14 06:23:06', '2017-04-14 06:23:11', '2017-04-14 06:23:11'),
(9, 'Adnan', '01.png', '2017-04-14 08:35:10', '2017-04-14 08:55:50', '2017-04-14 08:55:50'),
(10, '1', 'uploads/images/TczwvEKfzq6Wd7sq.png', '2017-04-14 08:56:02', '2017-04-14 08:56:03', NULL),
(11, '', 'uploads/images/FBzRjkGEzyMofl1D.png', '2017-04-14 08:56:09', '2017-04-14 08:56:09', NULL),
(12, '', 'uploads/images/AIrZUIYtXSy9qd73.png', '2017-04-14 08:56:20', '2017-04-14 08:56:20', NULL),
(13, '', 'uploads/images/yU65MmXosx2cTnN4.png', '2017-04-14 08:56:28', '2017-04-14 08:56:28', NULL),
(14, '', 'uploads/images/Rh2OpWPMrUYc1THd.png', '2017-04-14 08:56:35', '2017-04-14 08:56:35', NULL),
(15, '', 'uploads/images/jti3zEtjaTAmJnGe.png', '2017-04-14 08:56:42', '2017-04-14 08:56:43', NULL),
(16, '7', 'uploads/images/3aaJNoionTF0DEKI.png', '2017-04-14 08:57:24', '2017-04-14 08:57:24', NULL),
(17, '', 'uploads/images/76Oi2O2JxYF20mcK.png', '2017-04-14 08:57:35', '2017-04-14 08:57:35', NULL),
(18, '9', 'uploads/images/plZxClnXxz4Ubb67.png', '2017-04-14 09:28:21', '2017-04-14 09:28:21', NULL),
(19, '1', 'uploads/images/xUy4AZca7iY5ilbn.png', '2017-04-14 09:28:31', '2017-04-14 09:28:31', NULL),
(20, '11', 'uploads/images/px2AiZijL78dRFEE.png', '2017-04-14 09:28:42', '2017-04-14 09:28:42', NULL),
(21, '12', 'uploads/images/5rROydEg7wr78bra.png', '2017-04-14 09:28:52', '2017-04-14 09:28:52', NULL),
(22, '13', 'uploads/images/BLSC1Bt2VGk4C4mO.png', '2017-04-14 09:29:03', '2017-04-14 09:29:03', NULL),
(23, 'cocacola', 'uploads/images/SrDprN4awq8I02yY.png', '2017-04-14 09:29:17', '2017-04-14 09:29:17', NULL),
(24, 'pakarab', 'uploads/images/s6j8tyE6UCI2nt9k.png', '2017-04-14 09:29:32', '2017-04-14 09:29:32', NULL),
(25, 'parco', 'uploads/images/BIjEkPlpqJ82lI4y.png', '2017-04-14 09:29:44', '2017-04-14 09:29:44', NULL),
(26, 'pepsi', 'uploads/images/hWUwRTBGqYhaiCCq.png', '2017-04-14 09:29:56', '2017-04-14 09:29:56', NULL),
(27, 'star', 'uploads/images/Avh8OW5XEocfNwNm.png', '2017-04-14 09:30:08', '2017-04-14 09:30:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `phone3` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `email`, `phone1`, `phone2`, `address`, `created_at`, `updated_at`, `deleted_at`, `phone3`) VALUES
(1, 'adnanmumtazmayo@gmail.com', ' 0333-1234567', ' 0333-1234567', 'address', NULL, '2017-04-26 18:44:57', NULL, ' 0333-1234567');

-- --------------------------------------------------------

--
-- Table structure for table `download_books`
--

CREATE TABLE `download_books` (
  `id` int(10) unsigned NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `demo_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `original_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `download_books`
--

INSERT INTO `download_books` (`id`, `category`, `class`, `subject`, `demo_file`, `original_file`, `created_at`, `updated_at`, `deleted_at`, `image`) VALUES
(19, 'ADSAD', '5th', 'Chemistry', 'files/Adnan Mumtaz.pdf', '', '2017-03-18 14:55:49', '2017-03-18 14:55:49', NULL, NULL),
(20, 'Computer Science', '5th', 'Download', 'files/Course+Outline.pdf', 'files/Adnan Mumtaz.pdf', '2017-03-18 23:47:42', '2017-03-18 23:47:42', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `download_papers`
--

CREATE TABLE `download_papers` (
  `id` int(10) unsigned NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `demo_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `original_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chapter_or_term` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `download_papers`
--

INSERT INTO `download_papers` (`id`, `category`, `class`, `subject`, `demo_file`, `original_file`, `type`, `chapter_or_term`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Cateogry', 'Class', 'Chemistry', 'files/VisualCV_Examples_visualcv_resume.pdf', 'files/Adnan Mumtaz.pdf', 'Term', '5Th Chapter', 'uploads/images/RS7if5qsKy087Cph.jpg', '2017-03-19 02:24:32', '2017-03-19 02:24:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `image`, `category`, `published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'My GAllery', 'uploads/images/c7Nv9RLjsX8UgJSB.jpg', 'Testing', 1, '2016-12-26 12:57:26', '2016-12-26 12:57:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(10) unsigned NOT NULL,
  `type_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assets` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `type_id`, `user_id`, `entity_id`, `icon`, `class`, `text`, `assets`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 5, 'plus', 'bg-green', 'trans("history.backend.users.created") <strong>ADASDD</strong>', NULL, '2017-03-18 06:58:42', '2017-03-18 06:58:42'),
(2, 1, 1, 6, 'plus', 'bg-green', 'trans("history.backend.users.created") <strong>Adnan</strong>', NULL, '2017-03-18 07:01:07', '2017-03-18 07:01:07'),
(3, 1, 1, 7, 'plus', 'bg-green', 'trans("history.backend.users.created") <strong>Adnan</strong>', NULL, '2017-03-18 07:03:15', '2017-03-18 07:03:15'),
(4, 1, 1, 8, 'plus', 'bg-green', 'trans("history.backend.users.created") <strong>Adnan</strong>', NULL, '2017-03-18 07:04:37', '2017-03-18 07:04:37'),
(5, 1, 1, 9, 'plus', 'bg-green', 'trans("history.backend.users.created") <strong>Adnan</strong>', NULL, '2017-03-18 07:10:02', '2017-03-18 07:10:02'),
(6, 1, 1, 10, 'plus', 'bg-green', 'trans("history.backend.users.created") <strong>asdadad</strong>', NULL, '2017-03-18 07:11:24', '2017-03-18 07:11:24'),
(7, 1, 1, 11, 'plus', 'bg-green', 'trans("history.backend.users.created") <strong>Adnan Mumtaz</strong>', NULL, '2017-03-18 07:13:26', '2017-03-18 07:13:26'),
(8, 1, 1, 11, 'trash', 'bg-maroon', 'trans("history.backend.users.deleted") <strong>Adnan Mumtaz</strong>', NULL, '2017-03-18 14:23:17', '2017-03-18 14:23:17'),
(9, 1, 1, 10, 'trash', 'bg-maroon', 'trans("history.backend.users.deleted") <strong>asdadad</strong>', NULL, '2017-03-18 14:23:20', '2017-03-18 14:23:20'),
(10, 1, 1, 12, 'plus', 'bg-green', 'trans("history.backend.users.created") <strong>Adnan Mumtaz</strong>', NULL, '2017-03-18 14:24:48', '2017-03-18 14:24:48');

-- --------------------------------------------------------

--
-- Table structure for table `history_types`
--

CREATE TABLE `history_types` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `history_types`
--

INSERT INTO `history_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'User', '2016-12-25 03:41:08', '2016-12-25 03:41:08'),
(2, 'Role', '2016-12-25 03:41:08', '2016-12-25 03:41:08');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shor_description` text COLLATE utf8_unicode_ci NOT NULL,
  `long_description` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL,
  `featured` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `title`, `shor_description`, `long_description`, `slug`, `image`, `published`, `featured`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Awesome Room in Lahore', 'ADASAD', 'Hello', 'asdadad', 'uploads/images/rYkWNNNh7pFrXrno.png', 1, 1, '2017-04-25 17:11:55', '2017-04-25 17:38:06', '2017-04-25 17:38:06');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `maps`
--

CREATE TABLE `maps` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`, `id`) VALUES
('2014_10_12_000000_create_users_table', 1, 1),
('2014_10_12_100000_create_password_resets_table', 1, 2),
('2015_12_28_171741_create_social_logins_table', 1, 3),
('2015_12_29_015055_setup_access_tables', 1, 4),
('2016_07_03_062439_create_history_tables', 1, 5),
('2016_11_02_223020_create_banners_table', 1, 6),
('2016_11_08_192629_create_pages_table', 1, 7),
('2016_11_08_201140_add_heading_to_pages', 1, 8),
('2016_11_08_210142_create_locations_table', 1, 9),
('2016_11_08_213952_create_news_table', 1, 10),
('2016_11_09_063037_create_maps_table', 1, 11),
('2016_11_09_073033_create_galleries_table', 1, 12),
('2016_11_09_144254_create_contacts_table', 1, 13),
('2016_12_26_191634_create_sub_categories_table', 3, 15),
('2016_12_26_193703_create_books_table', 4, 16),
('2016_12_26_193917_add_sub_category_id_to_books', 5, 17),
('2016_12_26_200625_add_url_to_books', 6, 18),
('2016_12_27_062309_create_brands_table', 7, 19),
('2016_12_27_070715_create_reviews_table', 8, 20),
('2016_12_27_073323_add_featured_to_books', 9, 21),
('2017_02_25_194531_create_categories_table', 10, 22),
('2017_02_26_070541_add_category_id_to_users', 11, 23),
('2017_02_26_100245_add_parent_id_to_pages', 12, 24),
('2017_03_03_072731_create_download_books_table', 13, 26),
('2015_04_20_151900_create_uploads_table_zgldh', 14, 27),
('2015_11_18_132512_add_polymorphic_columns_to_uploads', 14, 28),
('2017_03_18_115230_add_image_to_users', 15, 29),
('2017_03_19_053507_add_image_to_download_books', 16, 36),
('2017_03_19_064929_create_download_papers_table', 16, 37),
('2017_04_14_074409_create_projects_table', 17, 38),
('2017_04_14_110439_create_clients_table', 18, 39),
('2017_04_14_145734_create_teams_table', 19, 41),
('2017_04_14_155109_create_project_details_table', 20, 42),
('2017_04_14_180453_create_certifications_table', 21, 43),
('2017_04_14_184505_add_phone_3_to_contacts', 22, 44),
('2017_04_21_234937_create_jobs_table', 23, 45),
('2017_04_25_215026_create_child_pages_table', 24, 46),
('2017_04_26_232521_create_quick_links_table', 25, 47),
('2017_04_26_235139_create_stories_table', 26, 49);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL,
  `heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `details`, `title`, `image`, `meta_description`, `slug`, `created_at`, `updated_at`, `deleted_at`, `heading`, `parent_id`) VALUES
(1, 'Home', '<p>This is awesome</p>\r\n', '', 'uploads/images/shKVZpcWjEdf3dn0.jpg', '', 'management', NULL, '2017-04-26 18:16:42', NULL, 'Management', 0),
(2, 'about us', '<p style="text-align: center;"><strong>Pan Islamic Industries (Pvt) Limited (PIIL)</strong>&nbsp;is committed to ensure the satisfaction of valued customers by providing them quality products on time at competitive prices, in doing so&nbsp;<strong>PIIL</strong>&nbsp;is determined to abide by QHSE Standard / regulations to ensure human health, operational safety, environmental protection and qauality system through continuous training, aligning the organization and audits.&nbsp;<strong>PIIL</strong>&nbsp;believes that through these efforts it is contributing towards economic development of the country.</p>\r\n', 'QHSE Policy', 'uploads/images/jOj9Vm35Sx3W3YlZ.jpg', '', 'QHSE-Policy', NULL, '2017-04-14 10:20:03', NULL, 'QHSE Policy', 0),
(3, 'Jobs', '<div class="col-md-8 col-md-offset-2">\r\n                                 <table class="table">\r\n								<thead>\r\n									<tr>\r\n										<th width="3%">#</th>\r\n										<th width="57%">List of Tools &amp; Machinery</th>\r\n										<th width="10%">Qty</th>\r\n										<th width="30%">Make</th>\r\n									</tr>\r\n								</thead>\r\n								<tbody>\r\n									<tr>\r\n									  <td>1</td>\r\n									  <td>210 Liter''s Drum Complete Plant</td>\r\n									  <td>1</td>\r\n									  <td>Imported</td>\r\n								  </tr>\r\n									<tr>\r\n									  <td>2</td>\r\n									  <td>Mobile Crane ( 10 Tons )</td>\r\n									  <td>1</td>\r\n									  <td>Imported</td>\r\n								  </tr>\r\n									<tr>\r\n									  <td>3</td>\r\n									  <td>Sheet Rolling &amp; Bundling Machines</td>\r\n									  <td>1</td>\r\n									  <td>Imported</td>\r\n								  </tr>\r\n									<tr>\r\n									  <td>4</td>\r\n									  <td>Submerge are welding machine</td>\r\n									  <td>1</td>\r\n									  <td>Imported</td>\r\n								  </tr>\r\n									<tr>\r\n									  <td>5</td>\r\n									  <td>CO2 Welding Plants</td>\r\n									  <td>1</td>\r\n									  <td>Imported</td>\r\n								  </tr>\r\n									<tr>\r\n									  <td>6</td>\r\n										<td>Lathe Machines 4'',6'',8'',10'', 12'' </td>\r\n										<td>5</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n									<tr>\r\n									  <td>7</td>\r\n										<td>Shaper 18", 24"</td>\r\n										<td>2</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n									<tr>\r\n									  <td>8</td>\r\n										<td>Universal Milling</td>\r\n										<td>1</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>9</td>\r\n										<td>Vertical Milling </td>\r\n										<td>1</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>10</td>\r\n										<td>Horizontal Milling</td>\r\n										<td>5</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>11</td>\r\n										<td>Surface Grinder</td>\r\n										<td>1</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>12</td>\r\n										<td>Hydraulic Presses</td>\r\n										<td>4</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>13</td>\r\n										<td>Schering Machine 5'', 8''</td>\r\n										<td>2</td>\r\n										<td>Local</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>14</td>\r\n										<td>Notching Machine</td>\r\n										<td>1</td>\r\n										<td>Local</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>15</td>\r\n										<td>Power Presses 15 – 200 Ton</td>\r\n										<td>15</td>\r\n										<td>Local</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>16</td>\r\n										<td>Vertical Drills</td>\r\n										<td>30</td>\r\n										<td>Local</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>17</td>\r\n										<td>Power Disc Cutter</td>\r\n										<td>10</td>\r\n										<td>Local</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>18</td>\r\n										<td>Welding Plants</td>\r\n										<td>30</td>\r\n										<td>Local / Imported</td>\r\n									</tr>\r\n                                     <tr>\r\n                                       <td>19</td>\r\n										<td>Spot Welding machine 50 KVA</td>\r\n										<td>11</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>20</td>\r\n										<td>Seam Welding Machine 150 KVA</td>\r\n										<td>1</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>21</td>\r\n										<td>Tank Sheet Rolling Machine</td>\r\n										<td>3</td>\r\n										<td>Local</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>22</td>\r\n										<td>Tube Bending Machine</td>\r\n										<td>1</td>\r\n										<td>Local</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>23</td>\r\n										<td>Tank Body Sizing Arrangement</td>\r\n										<td>1</td>\r\n										<td>Local</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>24</td>\r\n										<td>Tank Frame Bending Machine</td>\r\n										<td>1</td>\r\n										<td>Local</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>25</td>\r\n										<td>Tanks Testing System</td>\r\n										<td>1</td>\r\n										<td>Local</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>26</td>\r\n										<td>Shot Blasting Machine</td>\r\n										<td>1</td>\r\n										<td>Local</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>27</td>\r\n										<td>Galvanizing Unit 10''</td>\r\n										<td>1</td>\r\n										<td>Local</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>28</td>\r\n										<td>Galvanizing Unit 22''</td>\r\n										<td>1</td>\r\n										<td>Local</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>29</td>\r\n										<td>Heavy duty Compressors</td>\r\n										<td>3</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>30</td>\r\n										<td>Overhead Cranes   5 – 10 Ton</td>\r\n										<td>3</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>31</td>\r\n										<td>Generator 100 KVA</td>\r\n										<td>1</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>32</td>\r\n										<td>Hand Pallet Trolleys</td>\r\n										<td>2</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>33</td>\r\n										<td>Cutting Sheet Machine</td>\r\n										<td>1</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>34</td>\r\n										<td>Weighing Scale  2000 kg</td>\r\n										<td>1</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>35</td>\r\n										<td>Loader	Mazda</td>\r\n										<td>1</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>36</td>\r\n										<td>Factory tools</td>\r\n										<td>Complete</td>\r\n										<td>Imported</td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>37</td>\r\n										<td>Level with Tripod</td>\r\n										<td>4</td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>38</td>\r\n										<td>Mixer Machine (Full Bag)</td>\r\n										<td>2</td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>39</td>\r\n										<td>Vibrators</td>\r\n										<td>10</td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>40</td>\r\n										<td>Compactors</td>\r\n										<td>10</td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>41</td>\r\n										<td>Generator </td>\r\n										<td>4</td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>42</td>\r\n										<td>Light Trolley</td>\r\n										<td>4</td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>43</td>\r\n										<td>Water Tanker</td>\r\n										<td>4</td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>44</td>\r\n										<td>Measurement Box </td>\r\n										<td>Adequate Stock </td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>45</td>\r\n										<td>Cubes </td>\r\n										<td>Adequate Stock </td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>46</td>\r\n										<td>Wheel Barrows </td>\r\n										<td>1 </td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n										<td>47</td>\r\n										<td>Earth Resist Meter(Mugger) </td>\r\n										<td>4</td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n										<td>48</td>\r\n										<td>Shuttering(Steel Plates) </td>\r\n										<td>06 sets</td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n										<td>49</td>\r\n										<td>Template</td>\r\n										<td>06 sets</td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n										<td>50</td>\r\n										<td>Derrick Poles(Gin Pole)</td>\r\n										<td>4</td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n										<td>51</td>\r\n										<td>Pulley Blocks</td>\r\n										<td>Adequate Stock </td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n										<td>55</td>\r\n										<td>Chain Blocks</td>\r\n										<td>Adequate Stock </td>\r\n										<td></td>\r\n									</tr>\r\n                                    <tr>\r\n                                      <td>&nbsp;</td>\r\n                                      <td>&nbsp;</td>\r\n                                      <td>&nbsp;</td>\r\n                                      <td></td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                      <td>&nbsp;</td>\r\n                                      <td colspan="3"><strong>Bank Position of PAN ISLAMIC INDUSTRIES (PVT.) LTD.</strong></td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                      <td>&nbsp;</td>\r\n                                      <td colspan="3"><ul>\r\n                                        <li><span dir="LTR"> </span>Running Finance Facility of Rs. 250 Million</li>\r\n                                        <li><span dir="LTR"> </span>L/C Limit Facility of Rs. 450 Million</li>\r\n                                      </ul>\r\n                                        <p><strong>Pan  Islamic Industries (Pvt) Ltd. Works at Multan Road, Lahore-Pakistan is a state  of the art facility comprising of</strong></p>\r\n                                        <ul>\r\n                                        <ul>\r\n                                          <li><span dir="LTR"> </span>5 Acre Premises</li>\r\n                                          <li><span dir="LTR"> </span>Of which 150,000 square foot is covered area</li>\r\n                                          <li><span dir="LTR"> </span>Fabrication capacity 3000 M Tons per month</li>\r\n                                          <li><span dir="LTR"> </span>Galvanization Kettle and chrome nickel facility with  automatic temperature control </li>\r\n                                          <li><span dir="LTR"> </span>Professional and experienced work force</li>\r\n                                          <li><span dir="LTR"> </span>ISO 9000 based Quality Management System</li>\r\n                                          <li><span dir="LTR"> </span>Compliance to all relevant Product Standards and codes</li>\r\n                                          <li><span dir="LTR"> </span>Sandblasting Facility</li>\r\n                                        </ul>                                      </ul></td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                      <td>&nbsp;</td>\r\n                                      <td>&nbsp;</td>\r\n                                      <td>&nbsp;</td>\r\n                                      <td></td>\r\n                                    </tr>\r\n								</tbody>\r\n							</table>\r\n                          </div>', 'adasdasd', 'uploads/images/9fgigDf97cOeynr5.jpg', 'Assets', 'dsdfsdf', NULL, '2017-04-14 10:23:33', NULL, 'Assets', 0),
(4, 'Services', '', 'Project', 'uploads/images/YVoGqwYSCe51Zrjg.jpg', '', 'Project', NULL, '2017-04-14 10:33:20', NULL, 'Projects', 0),
(5, 'Procedure', '', 'Hello world', '', '', 'hello-world', NULL, '2016-12-25 14:07:00', NULL, '', 0),
(6, 'Contact Us', '<div class="col-md-6">\r\n<div class="panel-heading  bg-success">Head Office</div>\r\n\r\n<ul>\r\n	<li>FF 13, Leeds Center, Main Boulevard, Gulberg-III, Lahore-54000, Pakistan</li>\r\n	<li><a href="tel:9242357839379">+92 42 3578 3937-9</a></li>\r\n	<li>+92 42 3578 3939</li>\r\n	<li><a href="mailto:info@piil.pk">info@piil.pk</a></li>\r\n</ul>\r\n</div>\r\n\r\n<div class="col-md-6">\r\n<div class="panel-heading bg-info">Factory Site</div>\r\n\r\n<ul>\r\n	<li>Syed Irshad Ali Road, Near Zainbia Complex, Behind MAnsoora, 11 Km. Off Multan Road, Lahore - Pakistan.</li>\r\n	<li><a href="tel:924235420551">+92 42 3542 0551</a></li>\r\n	<li>+92 42 3542 6308</li>\r\n	<li><a href="mailto:info@piil.pk">info@piil.pk</a></li>\r\n</ul>\r\n</div>\r\n', 'Contact Us', 'uploads/images/6G9ZaLGhNemodDeu.jpg', '', 'contact-us', NULL, '2017-04-14 12:43:33', NULL, 'Contact Us', 0),
(29, 'news', 'testing', '', '', '', '', NULL, NULL, NULL, '', 0),
(30, 'MANPOWER RESOURCE SERVICES', '<p>lorem ipsum</p>\r\n', '', '', '', '', NULL, '2017-04-26 18:35:51', NULL, '', 0),
(31, 'footer', '<p>asdkjhakudhaksdja</p>\r\n', '', '', '', '', NULL, '2017-04-26 18:39:54', NULL, '', 0),
(32, 'virtual tour', ' <iframe width="100%" height="250" src="https://www.youtube.com/embed/hOznkOTlbWY"\r\n                                    frameborder="0" allowfullscreen></iframe>', '', '', '', '', NULL, '2017-04-26 19:34:17', NULL, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'view-backend', 'View Backend', 1, '2016-12-25 03:41:08', '2016-12-25 03:41:08'),
(2, 'manage-users', 'Manage Users', 2, '2016-12-25 03:41:08', '2016-12-25 03:41:08'),
(3, 'manage-roles', 'Manage Roles', 3, '2016-12-25 03:41:08', '2016-12-25 03:41:08');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) unsigned NOT NULL,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(1, 1, 2),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `homepage` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `image`, `homepage`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Adnan', 'uploads/images/nCfg5eK1p5w7ozIX.jpg', 1, '2017-04-14 02:51:15', '2017-04-14 02:51:15', NULL),
(2, '1', 'uploads/images/ITgVXYWAM1s9H5NS.png', 1, '2017-04-14 10:29:44', '2017-04-14 10:29:44', NULL),
(3, '2', 'uploads/images/MmhgEMXOmRFUvbvP.jpg', 0, '2017-04-14 10:29:53', '2017-04-14 10:29:54', NULL),
(4, '2', 'uploads/images/thhlBwep9Sw5j08o.png', 0, '2017-04-14 10:30:06', '2017-04-14 10:30:06', NULL),
(5, '3', 'uploads/images/q0FNWFVEe1nX1PHF.jpg', 1, '2017-04-14 10:30:24', '2017-04-14 10:30:24', NULL),
(6, '3', 'uploads/images/DWoPbvNmfwA0OHYS.png', 1, '2017-04-14 10:30:37', '2017-04-14 10:30:38', NULL),
(7, '4', 'uploads/images/p5dw96Igdtya7WIM.jpg', 1, '2017-04-14 10:30:49', '2017-04-14 10:30:49', NULL),
(8, '5', 'uploads/images/8o7J6OHx8ZSN0iLv.png', 1, '2017-04-14 10:31:01', '2017-04-14 10:31:02', NULL),
(9, '6', 'uploads/images/cJtIq2KTWVEsj3N7.jpg', 1, '2017-04-14 10:31:23', '2017-04-14 10:31:23', NULL),
(10, '7', 'uploads/images/WvKCHAl2SjIERgcT.jpg', 1, '2017-04-14 10:31:39', '2017-04-14 10:31:40', NULL),
(11, '8', 'uploads/images/L6gXylhYXenJHeuZ.png', 1, '2017-04-14 10:31:56', '2017-04-14 10:31:56', NULL),
(12, '12', 'uploads/images/Uqej3IY6tgS5fnOF.jpg', 1, '2017-04-14 10:32:12', '2017-04-14 10:32:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_details`
--

CREATE TABLE `project_details` (
  `id` int(10) unsigned NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `scope_of_work` text COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_details`
--

INSERT INTO `project_details` (`id`, `category`, `client`, `scope_of_work`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Sugar Mills', 'Rafhan Maize Products Co. Limited FAISALABAD', 'Overhead Crane of 50 Tons, Fabrication & Erection and installationCrystallizer. (Caramel Kettle acid Tanks, Co. Grit Cyclone, Bends, Steps, Sulphor Tower, Wooden Tan Tanks,Miracle & blender process dryers, Horpors, Con Sand Filters, Conveyor, Agitator, Pumps, Candles, Evaporator, fabrication & Erection of special Glucose Plant', '650.00 Millions', '2017-04-14 10:56:21', '2017-04-14 10:56:21', NULL),
(2, 'Sugar Mills', 'Heavy Mechanical Complex, TAXILA', 'Heavy Mechanical Complex Tanks for process etc', '5.5 Millions', '2017-04-14 10:57:51', '2017-04-14 10:57:51', NULL),
(3, 'Sugar Mills', 'Leiner Pak Gelatin Ltd. LAHORE', 'Erection of Boilers & Ducting etc', '2 Millions', '2017-04-14 10:58:31', '2017-04-14 10:58:31', NULL),
(4, 'Sugar Mills', 'ICI Pak. Ltd', 'Manufacturing & installation of Magma Stock Tank With Structure & Piping etc', '20 Millions', '2017-04-14 10:59:10', '2017-04-14 10:59:10', NULL),
(5, 'Sugar Mills', 'Pan Power International(Pvt.) Ltd. Lahore', 'Overhead Crane of 30 Tons, 02 Nos. Fabrication & Erection and installation Runway 250ft, with complete shed', '250 Millions', '2017-04-14 10:59:50', '2017-04-14 10:59:50', NULL),
(6, 'Sugar Mills', 'ADMORE GAS (PVT) LTD.', 'Manufacture & Supply of 5000 Gallons & 3000 Gallons StorTanks', '15 Millions', '2017-04-14 11:00:34', '2017-04-14 11:00:34', NULL),
(7, 'Sugar Mills', 'Pan Power International (Pvt.) Lhr.', 'Overhead Crane of 40 Tons, Fabrication & Erection and installation Runway 200ft, with complete shed', '235 Millions', '2017-04-14 11:01:05', '2017-04-14 11:01:05', NULL),
(8, 'Fabrication Projects ', 'D.G.P Army', 'Supply and Manufacturing of fire Extinguishers capacity 50 tons', '80 Millions', '2017-04-14 11:03:25', '2017-04-14 11:03:25', NULL),
(9, 'Fabrication Projects ', 'D.G.P Army', 'Supply of fire pump Trailers', '65 Millions', '2017-04-14 11:04:39', '2017-04-14 11:04:39', NULL),
(10, 'Fabrication Projects ', 'Atomic Energy', 'Fabrication and supply of steel form works, heavy/light duty fabrication, silos and overhead cranes', '275 Millions', '2017-04-14 11:06:16', '2017-04-14 11:06:16', NULL),
(11, 'Fabrication Projects ', 'Sino Tech Power Project', '1. Fabrication and Installations of Silos \r\n2. Fabrication of Beaching Plant Conveyors and Installations \r\n3. Fabrication of Steel Structure & Screw Conveyors & Installations', '270 Millions', '2017-04-14 11:08:27', '2017-04-14 11:08:27', NULL),
(12, 'Fabrication Projects ', 'Descon Engineering Limited', '1. DESIGN SUPPLY OF BATCHING PLANT CAPACITY 100 CUBIC METER FOR MANGLA DAM UPRAISING PROJECT •	QTY =04 NOS AMOUNTING RS.05 MILLIONS\r\n2.	DESIGN SUPPLY AND MANUFACTURING OF FLOATING PIPE STRUCTURE FOR MANGLA DAM UPRAISING PROJECT LENGTH 17 METER X 1.5 METER EACH •	QTY =50 NOS AMOUNTING RS.20 MILLION\r\n3. DESIGN SUPPLY AND MANUFACTURING OF PONTOONS SIZE: 6 METER X 1.5 METER EACH • QTY =300 NOS AMOUNTING RS.250 MILLION\r\n4-	DESIGN SUPPLY AND MANUFACTURING OF FLOATING CONVEYOR,GRATING,HOPPERS,BELT CONVEYORS AND ALLIED ACCESSORIIES • AMOUNTING RS.100 MILLIONS', '275 Millions', '2017-04-14 11:11:55', '2017-04-14 11:11:55', NULL),
(13, 'Fabrication Projects ', 'Emporium Center Nishat Chunian Project', 'Fabrication and Installations of 9 Nos. Cinema\r\nFabrication of Beaching Plant Conveyors and Installations\r\nFabrication of Silos & Installations', '380 Millions', '2017-04-14 11:12:38', '2017-04-14 11:12:38', NULL),
(14, 'Fabrication Projects ', 'N.L.C. Gujranwala Aziz Cross Pindi Bay Pass Project', 'Fabrication of Steel Shuttering (Pier & Transom)\r\nFabrication of Steel Shuttering JV Barriers\r\nFabrication of Steel Shuttering I-Girders\r\nFabrication of Steel Sign Boards\r\nFabrication of Steel Pipe Railing of Bridge', '560 Millions', '2017-04-14 11:14:17', '2017-04-14 11:14:17', NULL),
(15, 'Fabrication Projects ', 'N.L.C. Kacha Jail Road Kotlakhpat Lahore Project', 'Fabrication of Steel Shuttering (Pier & Transom)\r\n. Fabrication of Steel Shuttering I-Girders', '225 Millions', '2017-04-14 11:15:58', '2017-04-14 11:15:58', NULL),
(16, 'Fabrication Projects ', 'Metro Orange Train Project Package - II', 'Fabrication of Steel Shuttering (Pier & Transom)\r\nFabrication of Steel Work at Different Stations ( Running Project )', '690 Millions', '2017-04-14 11:17:41', '2017-04-14 11:17:41', NULL),
(17, 'Army Projects', 'Director General Procurment Army', 'Manufacturing and supply of Jerry cans, PSP Sheets, Picket angles e.t.c.', '500.00 Millions', '2017-04-14 11:18:38', '2017-04-14 11:18:38', NULL),
(18, 'Army Projects', 'Director General Procurment Army', 'Manufacture & Supply of 200 Liters & 45 Gallons 16 BG Barrels , Table Folding Legs Steel Tubing.', '300.00 Millions', '2017-04-14 11:19:18', '2017-04-14 11:19:18', NULL),
(19, 'Army Projects', 'Director General Procurment Air Force', 'Mobile Cranes QTY = 07 + 11 =18 Nos.', '150.00 Millions', '2017-04-14 11:20:04', '2017-04-14 11:20:04', NULL),
(20, 'Army Projects', 'Director General Procurment Air Force Kamra', 'Mobile Cranes QTY = 01 No.', '8.5 Millions', '2017-04-14 11:21:21', '2017-04-14 11:21:21', NULL),
(21, 'Army Projects', 'Director General Procurment Air Force', 'Diesel Engines with Transmission Qty = 30 Nos', '30.00 Millions', '2017-04-14 11:22:04', '2017-04-14 11:22:04', NULL),
(22, 'Army Projects', 'Director General Procurment POF', 'Ammunition Boxes (Different Sizes)', '100.00 Millions', '2017-04-14 11:22:36', '2017-04-14 11:22:36', NULL),
(23, 'Army Projects', 'ARDE Army', 'Dispensing Unit on Tank Fitted automation and manual system Supply', '95.00 Millions', '2017-04-14 11:23:15', '2017-04-14 11:23:15', NULL),
(24, 'Army Projects', 'ARDE Army', 'Oil Bearl 200 Gallon Powder Coated', '150.00 Millions', '2017-04-14 11:23:58', '2017-04-14 11:23:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quick_links`
--

CREATE TABLE `quick_links` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quick_links`
--

INSERT INTO `quick_links` (`id`, `name`, `color`, `link`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'All job', 'green', 'jobs', '2017-04-26 18:33:45', '2017-04-26 18:33:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `name`, `detail`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'aosiduyasudk', 'auisdya sudkhasj', 'uploads/images/tmfvOuSZ8fUP6KoP.png', '2016-12-27 02:14:58', '2016-12-27 02:14:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 1, 1, '2016-12-25 03:41:08', '2016-12-25 03:41:08'),
(2, 'Executive', 0, 2, '2016-12-25 03:41:08', '2016-12-25 03:41:08'),
(3, 'User', 0, 3, '2016-12-25 03:41:08', '2016-12-25 03:41:08');

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `provider` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE `stories` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stories`
--

INSERT INTO `stories` (`id`, `name`, `details`, `image`, `video`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'hello', 'Testign', 'uploads/images/t1ERwRzqIbxDifgS.png', ' <iframe width="100%" height="250" src="https://www.youtube.com/embed/hOznkOTlbWY"\r\n                                    frameborder="0" allowfullscreen></iframe>', '2017-04-26 19:28:35', '2017-04-26 19:28:36', NULL),
(2, 'rdxgfchgvj', '', '', '', NULL, NULL, NULL),
(3, 'dfgchgv', '', '', '', NULL, NULL, NULL),
(4, 'rdxgfchgvj', '', '', '', NULL, NULL, NULL),
(5, 'dfgchgv', '', '', '', NULL, NULL, NULL),
(6, 'rdxgfchgvj', '', '', '', NULL, NULL, NULL),
(7, 'dfgchgv', '', '', '', NULL, NULL, NULL),
(8, 'rdxgfchgvj', '', '', '', NULL, NULL, NULL),
(9, 'dfgchgv', '', '', '', NULL, NULL, NULL),
(10, 'rdxgfchgvj', '', '', '', NULL, NULL, NULL),
(11, 'dfgchgv', '', '', '', NULL, NULL, NULL),
(12, 'rdxgfchgvj', '', '', '', NULL, NULL, NULL),
(13, 'dfgchgv', '', '', '', NULL, NULL, NULL),
(14, 'rdxgfchgvj', '', '', '', NULL, NULL, NULL),
(15, 'dfgchgv', '', '', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `category_id`, `url`, `slug`, `title`, `meta_keywords`, `meta_description`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Adnan ', 1, 'asdadsadad', 'asdadsada dasdad', 'asdadasdad', 'sdasdadas', 'asdasidy aiduah', 'uploads/images/3vaPyeODzZBpab0e.jpg', '2016-12-26 14:20:39', '2016-12-27 00:15:24', NULL),
(2, 'asuydatsidg', 2, 'aiusdasudkh', 'asidukajhsd', 'askdjashd', 'askjdhaskjdh', 'aksdjhaksjdha', '', '2016-12-26 23:43:46', '2016-12-26 23:43:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `position`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Syed Shoaib Zahoor', 'CEO', 'uploads/images/lCLtRCvaEX5KZZJx.jpg', '2017-04-14 10:04:27', '2017-04-14 10:04:27', NULL),
(2, 'Syed Kashif Zzahoor', 'Managing Director', 'uploads/images/HlNW25MOIDGJQ7OR.jpg', '2017-04-14 10:05:00', '2017-04-14 10:05:00', NULL),
(3, 'Syed Shadab Zahoor', 'Director Operation', 'uploads/images/aId884BOqj81Wfv2.jpg', '2017-04-14 10:05:32', '2017-04-14 10:05:32', NULL),
(4, 'Syed Aftab Azmat', 'Director Projects & BD', 'uploads/images/FUH1T8GPAHhVk7pd.jpg', '2017-04-14 10:06:08', '2017-04-14 10:06:08', NULL),
(5, 'Mahmood ul Hassan', 'General Manager Marketing', 'uploads/images/izgVr2ttfJAEzPlm.jpg', '2017-04-14 10:06:45', '2017-04-14 10:06:45', NULL),
(6, 'Shahid Zia', 'CFO', 'uploads/images/84CgxR8U9AmoYq5O.jpg', '2017-04-14 10:07:27', '2017-04-14 10:07:27', NULL),
(7, 'Asif Nadeem', 'Manager Logestic & Aadmn', 'uploads/images/yXMlg1lo7wXBQ5Xl.jpg', '2017-04-14 10:07:47', '2017-04-14 10:14:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `uploadable_id` int(11) DEFAULT NULL,
  `uploadable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `confirmation_code`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `image`) VALUES
(1, 'Admin Istrator', 'admin@admin.com', '$2y$10$3AfKpJ5RoUSEB88uO72GmOBFS2CTUNffHYAji2Xn5rzZnkL.3Fcd2', 1, '0183c05d1afe3578fab2026e693112c6', 1, 'tBFWzx4ZySk0XRWmEOurQSX9kYmHni3ufvs9I9ZjHXI5uTHex52qaHDf2rSM', '2016-12-25 03:41:08', '2017-04-22 20:18:15', NULL, 'uploads/images/mEXvRbRjqBAVdTPk.jpg'),
(2, 'Backend User', 'executive@executive.com', '$2y$10$/jke5cAHlQ62.GXwMU5NeeWGJR/LZX9tpGHE1IY08JM8a5.oLwgUC', 1, '753810144a8b4d71e771d1ac1ecc9842', 1, NULL, '2016-12-25 03:41:08', '2016-12-25 03:41:08', NULL, NULL),
(3, 'Default User', 'user@user.com', '$2y$10$WUVlXxpFhWPVRkLLtS15MOHPj5mpMVPxeOtzPBPtFGGJso2UdDElO', 1, '430d56dfcdcf2e1b371b8f9c6335712d', 1, NULL, '2016-12-25 03:41:08', '2016-12-25 03:41:08', NULL, NULL),
(12, 'Adnan Mumtaz', 'adnanmumtazmayo1@gmail.com', '$2y$10$kZfJ9Igh7ujzzdH7k6TpEuuFMStDZJdpFY3zCOo2zOMpaeOkIDIWa', 1, 'fe9630a460e6d7116a2514ceae21d74f', 1, NULL, '2017-03-18 14:24:48', '2017-03-18 14:34:45', NULL, 'uploads/images/si1MLmxaLuDzgvcf.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assigned_roles`
--
ALTER TABLE `assigned_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assigned_roles_user_id_foreign` (`user_id`),
  ADD KEY `assigned_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `certifications`
--
ALTER TABLE `certifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `child_pages`
--
ALTER TABLE `child_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `download_books`
--
ALTER TABLE `download_books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `download_papers`
--
ALTER TABLE `download_papers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_type_id_foreign` (`type_id`),
  ADD KEY `history_user_id_foreign` (`user_id`);

--
-- Indexes for table `history_types`
--
ALTER TABLE `history_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maps`
--
ALTER TABLE `maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_details`
--
ALTER TABLE `project_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_links`
--
ALTER TABLE `quick_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `stories`
--
ALTER TABLE `stories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploadable_index` (`uploadable_id`,`uploadable_type`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assigned_roles`
--
ALTER TABLE `assigned_roles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `certifications`
--
ALTER TABLE `certifications`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `child_pages`
--
ALTER TABLE `child_pages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `download_books`
--
ALTER TABLE `download_books`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `download_papers`
--
ALTER TABLE `download_papers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `history_types`
--
ALTER TABLE `history_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `maps`
--
ALTER TABLE `maps`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `project_details`
--
ALTER TABLE `project_details`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `quick_links`
--
ALTER TABLE `quick_links`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stories`
--
ALTER TABLE `stories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `assigned_roles`
--
ALTER TABLE `assigned_roles`
  ADD CONSTRAINT `assigned_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assigned_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `history_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
