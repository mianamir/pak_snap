@extends('backend.layouts.master')

@section('content')
    <section class="content-header">
        <h1>
            Book
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                {!! Form::model($book, ['route' => ['admin.books.update', $book->id], 'method' => 'patch', 'files' => true]) !!}

                <!-- Name Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('name', 'Name:') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Book Code Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('book_code', 'Book Code:') !!}
                        {!! Form::text('book_code', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Total Pages Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('total_pages', 'Total Pages:') !!}
                        {!! Form::text('total_pages', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Price Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('price', 'Price:') !!}
                        {!! Form::text('price', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Author Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('author', 'Author:') !!}
                        {!! Form::text('author', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Publishers Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('publishers', 'Publishers:') !!}
                        {!! Form::text('publishers', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group col-sm-6">
                        {!! Form::label('url', 'URL:') !!}
                        {!! Form::text('url', null, ['class' => 'form-control']) !!}
                    </div>
                    <!-- Title Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('title', 'Title:') !!}
                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Meta Description Field -->
                    <div class="form-group col-sm-12 col-lg-12">
                        {!! Form::label('meta_description', 'Meta Description:') !!}
                        {!! Form::textarea('meta_description', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Meta Keywords Field -->
                    <div class="form-group col-sm-12 col-lg-12">
                        {!! Form::label('meta_keywords', 'Meta Keywords:') !!}
                        {!! Form::textarea('meta_keywords', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group col-sm-12 col-lg-12">
                        {!! Form::label('sub_category_id', 'Sub_Category_id:') !!}

                        <select class="form-control" name="category_id">
                            <?php
                            $categories = \App\Models\Admin\Category::all();
                            ?>
                            @foreach( $categories as $category)
                                <option value="{{$category->id}}" {{$category->id == $book->sub_category_id? "selected":""}}>{{$category->name}}</option>
                            @endforeach

                        </select>

                    </div>

                    <!-- Image Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('image', 'Image:') !!}
                        {!! Form::file('image') !!}
                    </div>
                    <div class="clearfix"></div>

                    <!-- Demo File Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('demo_file', 'Demo File:') !!}
                        {!! Form::file('demo_file') !!}
                    </div>
                    <div class="clearfix"></div>

                    <!-- Pdf File Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('pdf_file', 'Pdf File:') !!}
                        {!! Form::file('pdf_file') !!}
                    </div>
                    <div class="clearfix"></div>

                    <div class="form-group col-sm-6">
                        {!! Form::label('featured', 'featured:') !!}
                        <label class="checkbox-inline">
                            {!! Form::hidden('featured', 0) !!}
                            {!! Form::checkbox('featured', 1, null) !!} yes
                        </label>
                    </div>

                    <!-- Published Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('published', 'Published:') !!}
                        <label class="checkbox-inline">
                            {!! Form::hidden('published', 0) !!}
                            {!! Form::checkbox('published', 1, null) !!} yes
                        </label>
                    </div>

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('admin.books.index') !!}" class="btn btn-default">Cancel</a>
                    </div>


                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection