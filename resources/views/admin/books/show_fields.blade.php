<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $book->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $book->name !!}</p>
</div>

<!-- Book Code Field -->
<div class="form-group">
    {!! Form::label('book_code', 'Book Code:') !!}
    <p>{!! $book->book_code !!}</p>
</div>

<!-- Total Pages Field -->
<div class="form-group">
    {!! Form::label('total_pages', 'Total Pages:') !!}
    <p>{!! $book->total_pages !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $book->price !!}</p>
</div>

<!-- Author Field -->
<div class="form-group">
    {!! Form::label('author', 'Author:') !!}
    <p>{!! $book->author !!}</p>
</div>

<!-- Publishers Field -->
<div class="form-group">
    {!! Form::label('publishers', 'Publishers:') !!}
    <p>{!! $book->publishers !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $book->title !!}</p>
</div>

<!-- Meta Description Field -->
<div class="form-group">
    {!! Form::label('meta_description', 'Meta Description:') !!}
    <p>{!! $book->meta_description !!}</p>
</div>

<!-- Meta Keywords Field -->
<div class="form-group">
    {!! Form::label('meta_keywords', 'Meta Keywords:') !!}
    <p>{!! $book->meta_keywords !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $book->image !!}</p>
</div>

<!-- Demo File Field -->
<div class="form-group">
    {!! Form::label('demo_file', 'Demo File:') !!}
    <p>{!! $book->demo_file !!}</p>
</div>

<!-- Pdf File Field -->
<div class="form-group">
    {!! Form::label('pdf_file', 'Pdf File:') !!}
    <p>{!! $book->pdf_file !!}</p>
</div>

<!-- Published Field -->
<div class="form-group">
    {!! Form::label('published', 'Published:') !!}
    <p>{!! $book->published !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $book->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $book->updated_at !!}</p>
</div>

