@extends('backend.layouts.master')

@section('content')
    <section class="content-header">
        <h1>
            Child Pages
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($childPages, ['route' => ['admin.childPages.update', $childPages->id], 'method' => 'patch','files'=>true]) !!}

                        @include('admin.child_pages.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection