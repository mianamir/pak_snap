<table class="table table-responsive" id="childPages-table">
    <thead>
        <th>Name</th>
        <th>Title</th>
        <th>Page Id</th>

        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($childPages as $childPages)
        <tr>
            <td>{!! $childPages->name !!}</td>
            <td>{!! $childPages->title !!}</td>
            <td>{!! $childPages->page_id !!}</td>

            <td>
                {!! Form::open(['route' => ['admin.childPages.destroy', $childPages->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.childPages.show', [$childPages->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.childPages.edit', [$childPages->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>