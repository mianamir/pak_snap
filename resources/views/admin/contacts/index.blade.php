@extends('backend.layouts.master')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Contacts</h1>
        <h1 class="pull-right">
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.contacts.table')
            </div>
        </div>
    </div>
@endsection

