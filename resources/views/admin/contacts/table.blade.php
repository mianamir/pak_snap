<table class="table table-responsive" id="contacts-table">
    <thead>
    <th>Email</th>
    <th>Phone1</th>
    <th>Phone2</th>
    <th>Address</th>
    <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($contacts as $contact)
        <tr>
            <td>{!! $contact->email !!}</td>
            <td>{!! $contact->phone1 !!}</td>
            <td>{!! $contact->phone2 !!}</td>
            <td>{!! $contact->address !!}</td>
            <td>
                <div class='btn-group'>
                    <a href="{!! route('admin.contacts.show', [$contact->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.contacts.edit', [$contact->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>