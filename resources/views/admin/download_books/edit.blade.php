@extends('backend.layouts.master')

@section('content')
    <section class="content-header">
        <h1>
            Download Book
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($downloadBook, ['route' => ['admin.downloadBooks.update', $downloadBook->id], 'method' => 'patch','files'=>true]) !!}

                        @include('admin.download_books.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection