@extends('backend.layouts.master')

@section('content')
    <section class="content-header">
        <h1>
            Download Paper
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($downloadPaper, ['route' => ['admin.downloadPapers.update', $downloadPaper->id], 'method' => 'patch','files' => true ]) !!}

                        @include('admin.download_papers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection