<!-- Cateogry Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cateogry', 'Cateogry:') !!}
    {!! Form::text('category', null, ['class' => 'form-control']) !!}
</div>

<!-- Class Field -->
<div class="form-group col-sm-6">
    {!! Form::label('class', 'Class:') !!}
    {!! Form::text('class', null, ['class' => 'form-control']) !!}
</div>

<!-- Subject Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subject', 'Subject:') !!}
    {!! Form::text('subject', null, ['class' => 'form-control']) !!}
</div>

<!-- Demo File Field -->
<div class="form-group col-sm-6">
    {!! Form::label('demo_file', 'Demo File:') !!}
    {!! Form::file('demo_file') !!}
</div>
<div class="clearfix"></div>

<!-- Original File Field -->
<div class="form-group col-sm-6">
    {!! Form::label('original_file', 'Original File:') !!}
    {!! Form::file('original_file') !!}
</div>
<div class="clearfix"></div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Chapter Or Term Field -->
<div class="form-group col-sm-6">
    {!! Form::label('chapter_or_term', 'Chapter Or Term:') !!}
    {!! Form::text('chapter_or_term', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.downloadPapers.index') !!}" class="btn btn-default">Cancel</a>
</div>
