<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $downloadPaper->id !!}</p>
</div>

<!-- Cateogry Field -->
<div class="form-group">
    {!! Form::label('cateogry', 'Cateogry:') !!}
    <p>{!! $downloadPaper->cateogry !!}</p>
</div>

<!-- Class Field -->
<div class="form-group">
    {!! Form::label('class', 'Class:') !!}
    <p>{!! $downloadPaper->class !!}</p>
</div>

<!-- Subject Field -->
<div class="form-group">
    {!! Form::label('subject', 'Subject:') !!}
    <p>{!! $downloadPaper->subject !!}</p>
</div>

<!-- Demo File Field -->
<div class="form-group">
    {!! Form::label('demo_file', 'Demo File:') !!}
    <p>{!! $downloadPaper->demo_file !!}</p>
</div>

<!-- Original File Field -->
<div class="form-group">
    {!! Form::label('original_file', 'Original File:') !!}
    <p>{!! $downloadPaper->original_file !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $downloadPaper->type !!}</p>
</div>

<!-- Chapter Or Term Field -->
<div class="form-group">
    {!! Form::label('chapter_or_term', 'Chapter Or Term:') !!}
    <p>{!! $downloadPaper->chapter_or_term !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $downloadPaper->image !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $downloadPaper->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $downloadPaper->updated_at !!}</p>
</div>

