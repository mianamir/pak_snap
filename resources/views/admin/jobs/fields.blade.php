<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Shor Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('shor_description', 'Shor Description:') !!}
    {!! Form::textarea('shor_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Long Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('long_description', 'Long Description:') !!}
    {!! Form::textarea('long_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image') !!}
</div>
<div class="clearfix"></div>

<!-- Published Field -->
<div class="form-group col-sm-6">
    {!! Form::label('published', 'Published:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('published', false) !!}
        {!! Form::checkbox('published', '1', null) !!} 1
    </label>
</div>

<!-- Featured Field -->
<div class="form-group col-sm-6">
    {!! Form::label('featured', 'Featured:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('featured', false) !!}
        {!! Form::checkbox('featured', 1, null) !!} yes
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.jobs.index') !!}" class="btn btn-default">Cancel</a>
</div>
