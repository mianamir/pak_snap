<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $job->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $job->title !!}</p>
</div>

<!-- Shor Description Field -->
<div class="form-group">
    {!! Form::label('shor_description', 'Shor Description:') !!}
    <p>{!! $job->shor_description !!}</p>
</div>

<!-- Long Description Field -->
<div class="form-group">
    {!! Form::label('long_description', 'Long Description:') !!}
    <p>{!! $job->long_description !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $job->slug !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $job->image !!}</p>
</div>

<!-- Published Field -->
<div class="form-group">
    {!! Form::label('published', 'Published:') !!}
    <p>{!! $job->published !!}</p>
</div>

<!-- Featured Field -->
<div class="form-group">
    {!! Form::label('featured', 'Featured:') !!}
    <p>{!! $job->featured !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $job->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $job->updated_at !!}</p>
</div>

