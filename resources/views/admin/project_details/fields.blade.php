<!-- Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category', 'Category:') !!}
    {!! Form::text('category', null, ['class' => 'form-control']) !!}
</div>

<!-- Client Field -->
<div class="form-group col-sm-6">
    {!! Form::label('client', 'Client:') !!}
    {!! Form::text('client', null, ['class' => 'form-control']) !!}
</div>

<!-- Scope Of Work Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('scope_of_work', 'Scope Of Work:') !!}
    {!! Form::textarea('scope_of_work', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.projectDetails.index') !!}" class="btn btn-default">Cancel</a>
</div>
