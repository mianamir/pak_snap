<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $projectDetail->id !!}</p>
</div>

<!-- Category Field -->
<div class="form-group">
    {!! Form::label('category', 'Category:') !!}
    <p>{!! $projectDetail->category !!}</p>
</div>

<!-- Client Field -->
<div class="form-group">
    {!! Form::label('client', 'Client:') !!}
    <p>{!! $projectDetail->client !!}</p>
</div>

<!-- Scope Of Work Field -->
<div class="form-group">
    {!! Form::label('scope_of_work', 'Scope Of Work:') !!}
    <p>{!! $projectDetail->scope_of_work !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $projectDetail->price !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $projectDetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $projectDetail->updated_at !!}</p>
</div>

