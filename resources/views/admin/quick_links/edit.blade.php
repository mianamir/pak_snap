@extends('backend.layouts.master')

@section('content')
    <section class="content-header">
        <h1>
            Quick Link
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($quickLink, ['route' => ['admin.quickLinks.update', $quickLink->id], 'method' => 'patch']) !!}

                        @include('admin.quick_links.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection