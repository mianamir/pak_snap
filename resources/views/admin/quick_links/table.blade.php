<table class="table table-responsive" id="quickLinks-table">
    <thead>
        <th>Name</th>
        <th>Color</th>
        <th>Link</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($quickLinks as $quickLink)
        <tr>
            <td>{!! $quickLink->name !!}</td>
            <td>{!! $quickLink->color !!}</td>
            <td>{!! $quickLink->link !!}</td>
            <td>{!! $quickLink->created_at !!}</td>
            <td>{!! $quickLink->updated_at !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.quickLinks.destroy', $quickLink->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.quickLinks.show', [$quickLink->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.quickLinks.edit', [$quickLink->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>