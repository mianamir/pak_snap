@extends('backend.layouts.master')

@section('content')
    <section class="content-header">
        <h1>
            Review
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($review, ['route' => ['admin.reviews.update', $review->id], 'method' => 'patch' , 'files'=>true]) !!}

                        @include('admin.reviews.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection