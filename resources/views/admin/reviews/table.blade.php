<table class="table table-responsive" id="reviews-table">
    <thead>
        <th>Name</th>
        <th>Image</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($reviews as $review)
        <tr>
            <td>{!! $review->name !!}</td>
            <td>{!! $review->image !!}</td>
            <td>{!! $review->created_at !!}</td>
            <td>{!! $review->updated_at !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.reviews.destroy', $review->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.reviews.show', [$review->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.reviews.edit', [$review->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>