<table class="table table-responsive" id="stories-table">
    <thead>
        <th>Name</th>
        <th>Image</th>

        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($stories as $story)
        <tr>
            <td>{!! $story->name !!}</td>
            <td> <img src="{{asset($story->image )}}" style="width: 100px;height: 100px"/> </td>

            <td>
                {!! Form::open(['route' => ['admin.stories.destroy', $story->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.stories.show', [$story->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.stories.edit', [$story->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>