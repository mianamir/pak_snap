<table class="table table-responsive" id="teams-table">
    <thead>
        <th>Name</th>
        <th>Position</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($teams as $team)
        <tr>
            <td>{!! $team->name !!}</td>
            <td>{!! $team->position !!}</td>
            <td>{!! $team->created_at !!}</td>
            <td>{!! $team->updated_at !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.teams.destroy', $team->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.teams.show', [$team->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.teams.edit', [$team->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>