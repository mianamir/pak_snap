<!-- Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Filename Field -->
<div class="form-group col-sm-6">
    {!! Form::label('filename', 'Filename:') !!}
    {!! Form::text('filename', null, ['class' => 'form-control']) !!}
</div>

<!-- Original Filedir Field -->
<div class="form-group col-sm-6">
    {!! Form::label('original_filedir', 'Original Filedir:') !!}
    {!! Form::text('original_filedir', null, ['class' => 'form-control']) !!}
</div>

<!-- Original Extension Field -->
<div class="form-group col-sm-6">
    {!! Form::label('original_extension', 'Original Extension:') !!}
    {!! Form::text('original_extension', null, ['class' => 'form-control']) !!}
</div>

<!-- Original Width Field -->
<div class="form-group col-sm-6">
    {!! Form::label('original_width', 'Original Width:') !!}
    {!! Form::text('original_width', null, ['class' => 'form-control']) !!}
</div>

<!-- Original Height Field -->
<div class="form-group col-sm-6">
    {!! Form::label('original_height', 'Original Height:') !!}
    {!! Form::text('original_height', null, ['class' => 'form-control']) !!}
</div>

<!-- Dir Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dir', 'Dir:') !!}
    {!! Form::text('dir', null, ['class' => 'form-control']) !!}
</div>

<!-- Original Mime Field -->
<div class="form-group col-sm-6">
    {!! Form::label('original_mime', 'Original Mime:') !!}
    {!! Form::text('original_mime', null, ['class' => 'form-control']) !!}
</div>

<!-- Alt Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alt_text', 'Alt Text:') !!}
    {!! Form::text('alt_text', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Basename Field -->
<div class="form-group col-sm-6">
    {!! Form::label('basename', 'Basename:') !!}
    {!! Form::text('basename', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.userImages.index') !!}" class="btn btn-default">Cancel</a>
</div>
