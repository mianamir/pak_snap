<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $userImage->id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $userImage->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $userImage->updated_at !!}</p>
</div>

<!-- Filename Field -->
<div class="form-group">
    {!! Form::label('filename', 'Filename:') !!}
    <p>{!! $userImage->filename !!}</p>
</div>

<!-- Original Filedir Field -->
<div class="form-group">
    {!! Form::label('original_filedir', 'Original Filedir:') !!}
    <p>{!! $userImage->original_filedir !!}</p>
</div>

<!-- Original Extension Field -->
<div class="form-group">
    {!! Form::label('original_extension', 'Original Extension:') !!}
    <p>{!! $userImage->original_extension !!}</p>
</div>

<!-- Original Width Field -->
<div class="form-group">
    {!! Form::label('original_width', 'Original Width:') !!}
    <p>{!! $userImage->original_width !!}</p>
</div>

<!-- Original Height Field -->
<div class="form-group">
    {!! Form::label('original_height', 'Original Height:') !!}
    <p>{!! $userImage->original_height !!}</p>
</div>

<!-- Dir Field -->
<div class="form-group">
    {!! Form::label('dir', 'Dir:') !!}
    <p>{!! $userImage->dir !!}</p>
</div>

<!-- Original Mime Field -->
<div class="form-group">
    {!! Form::label('original_mime', 'Original Mime:') !!}
    <p>{!! $userImage->original_mime !!}</p>
</div>

<!-- Alt Text Field -->
<div class="form-group">
    {!! Form::label('alt_text', 'Alt Text:') !!}
    <p>{!! $userImage->alt_text !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $userImage->user_id !!}</p>
</div>

<!-- Basename Field -->
<div class="form-group">
    {!! Form::label('basename', 'Basename:') !!}
    <p>{!! $userImage->basename !!}</p>
</div>

