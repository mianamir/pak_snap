<table class="table table-responsive" id="userImages-table">
    <thead>
        <th>Id</th>
        <th>Filename</th>
        <th>Original Filedir</th>
        <th>Original Extension</th>
        <th>Original Width</th>
        <th>Original Height</th>
        <th>Dir</th>
        <th>Original Mime</th>
        <th>Alt Text</th>
        <th>User Id</th>
        <th>Basename</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($userImages as $userImage)
        <tr>
            <td>{!! $userImage->id !!}</td>
            <td>{!! $userImage->filename !!}</td>
            <td>{!! $userImage->original_filedir !!}</td>
            <td>{!! $userImage->original_extension !!}</td>
            <td>{!! $userImage->original_width !!}</td>
            <td>{!! $userImage->original_height !!}</td>
            <td>{!! $userImage->dir !!}</td>
            <td>{!! $userImage->original_mime !!}</td>
            <td>{!! $userImage->alt_text !!}</td>
            <td>{!! $userImage->user_id !!}</td>
            <td>{!! $userImage->basename !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.userImages.destroy', $userImage->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.userImages.show', [$userImage->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.userImages.edit', [$userImage->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>