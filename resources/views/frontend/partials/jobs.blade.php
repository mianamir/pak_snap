<?php
$jobs = \App\Models\Admin\Job::all();

?>

@foreach($jobs as $job)
    <div class="highlight">
        <a href="{{route('jobsDetail',[$job->slug])}}">
            <div class="alert-img"><img src="{{asset($job->image)}}" class="img-responsive"/>
            </div>

            <div class="alert-text">
                <h4>{{$job->title}}</h4>
                {!! $job->shor_description !!}
            </div>
        </a>
    </div> <!--highlight ends-->
@endforeach