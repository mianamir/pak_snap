<div class="col-md-7">


    <form class="form-light mt-20" role="form" method=post action="{{url('send-cv')}}"  enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Your name" name="form[Name]" required>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email address" name="form[email]" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Phone number" name="form[Phone]" required>
                </div>
            </div>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Cell No" name="form[Cell No]" required>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Position Applied For" name="form[Position]" required>
        </div>
        <div class="form-group">
            <input type="file" name="file"  required>
        </div>
        <div class="form-group">
            <textarea class="form-control" placeholder="Address" style="height:100px;" name="form[Address]"></textarea>
        </div>
        <div class="form-group">
            <textarea class="form-control" placeholder="Write you message here..." style="height:100px;" name="form[Details]"></textarea>
        </div>
        <button type="submit" class="btn btn-base">Send message</button>
    </form>
</div>
