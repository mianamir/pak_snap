@extends('frontend.layouts.pak')

@section('content')
    <!-- section start -->
    <!-- ================ -->
    <section class="pv-30 clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">Get In Touch</h1>
                    <div class="separator"></div>
                    <p class="text-center">{!! $page->details !!}</p>
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="list-icons">
                                <?php
                                $contact = \App\Models\Admin\Contact::where('id',1)->first();
                                ?>
                                <li><i class="fa fa-map-marker pr-10"></i> {{$contact->address}}</li>
                                <li><i class="fa fa-phone pr-10"></i> {{$contact->phone1}}</li>
                                <li><i class="fa fa-mobile pr-10"></i> {{$contact->phone2}}</li>
                                <li><a href="mailto:info@paksnaps.com"><i class="fa fa-envelope-o pr-10"></i>{{$contact->email}}</a></li>
                            </ul>

                        </div>
                        <div class="col-md-6">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27224.682898817162!2d74.27370726762818!3d31.466837908769484!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3919015f82b0b86f%3A0x2fcaf9fdeb3d02e6!2sJohar+Town%2C+Lahore%2C+Pakistan!5e0!3m2!1sen!2s!4v1494954961348" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section end -->

@endsection