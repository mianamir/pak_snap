@extends('frontend.layouts.lasani')

@section('content')

    <!-- banner start -->
    <!-- ================ -->

    <!-- banner end -->

    <div id="page-start"></div>

    <!-- section start -->
    <!-- ================ -->
    <section class="video-background" style="margin-top:10px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center" data-animation-effect="zoomIn"
                        data-effect-delay="100">Gallery Images</h2>
                    <div class="separator object-non-visible" data-animation-effect="zoomIn"
                         data-effect-delay="100"></div>

                </div>
            </div>
        </div>
    </section>
    <!-- section end -->
    <!-- section start -->
    <!-- ================ -->
    <section class="section clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- -->


                    <div class="col-md-12">
                        <div class="row grid-space-0">

                            <div class="col-md-12">
                                <div class="row grid-space-0">
                                    <div class="gallery-page">
                                        <div class="row margin-bottom-20">
                                            @foreach($images as $image)
                                                <div class="col-md-4 col-sm-4">
                                                    <a class="thumbnail fancybox-button zoomer"
                                                       data-rel="fancybox-button" title="Project Title"
                                                       href="{{asset($image->image)}}">
                                                        <span class="overlay-zoom">
                                                            <img alt=""
                                                                 src="{{asset($image->image)}}"
                                                                 class="img-responsive">
                                                            <span class="zoom-icon"></span>
                                                        </span>
                                                    </a>
                                                    <h5 align="center">{{$image->name}}</h5>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection