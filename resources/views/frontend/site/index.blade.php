@extends('frontend.layouts.pak')

@section('content')

    <!-- banner start -->
    <div class="tp-banner-container">


        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">

                <?php

                $banners = \App\Models\Admin\Banner::all();
                $counter = 0;
                ?>
                @foreach($banners as $banner)
                <div class="item {{$counter++==0?"active":""}}">
                    <img src="{{asset($banner->image)}}"  alt=""/>
                </div>
                @endforeach


            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

    </div>
    <!--=== End Slider ===-->

    <div id="page-start"></div>

    <!-- section start -->
    <!-- ================ -->
    <section class="light-gray-bg pv-30 clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <!--<h1 class="text-center">Pak Snaps</h1>
                    <div class="separator"></div>-->
                    <?php
                    $page = \App\Models\Admin\Page::where('name','home')->first();
                    ?>
                    <p class="text-center">{!! $page->details !!}</p>
                </div>
            </div>
        </div>
    </section>
    <!-- section end -->

    <!-- section start -->
    <!-- ================ -->
    <section class="full-width-section">
        <div class="full-image-container default-bg">
            <?php
            $page1 = \App\Models\Admin\Page::where('name','home-image-slider-text')->first();
            ?>
            <img class="to-right-block" src="{{asset($page1->image)}}" alt="">
            <div class="full-image-overlay text-center">
                <h3>Pak <i class="fa fa-camera"></i> Snaps</h3>
                <p>{!! $page->details!!}</p>
                <ul class="social-links circle animated-effect-1 text-center">
                    <li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a target="_blank" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
                    <li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="full-text-container default-bg">
            <?php
            $page2 = \App\Models\Admin\Page::where('name','home-send-message')->first();
            ?>
            <h2>{{$page2->title}}</h2>
            <div class="separator-2 visible-lg"></div>
            <p>{!! $page->details!!}</p>
            <div class="separator-3 visible-lg"></div>
        </div>
    </section>
    <!-- section end -->


    <!-- section start -->
    <!-- ================ -->
    <section class="pv-30 clearfix">
        <div class="container">
            <div class="row">
                <h2 class="text-center">Photo Gallery</h2>
                <div class="separator"></div>
                <p class="lead text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            </div>
            <!-- isotope filters start -->

            <!-- isotope filters end -->
        </div>
        <div class="isotope-container row grid-space-0">
            <?php
            $hgs = \App\Models\Admin\Project::where('homepage',0)->get();

            $counter = 0;
            $counter1 = 0;
            ?>
            @foreach($hgs as $hg )
            <div class="col-sm-6 col-md-3 isotope-item {{$counter++==0?"the-bride":""}}">
                <div id="carousel-portfolio" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators top">
                        <li data-target="#carousel-portfolio" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-portfolio" data-slide-to="1"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">

                        <div class="item {{$counter1++==0?"active":""}}">
                            <div class="image-box text-center">
                                <div class="overlay-container overlay-visible">
                                    <img src="{{asset($hg->image)}}" alt="">
                                    <a href="{{asset($hg->image)}}" class="popup-img overlay-link" title="image caption"><i class="fa fa-plus"></i></a>
                                    <div class="overlay-bottom hidden-xs">
                                        <div class="text">
                                            <p class="lead margin-clear">{{$hg->name}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            @endforeach

        </div>
        <div class="container">
            <div class="row">
                <h2 class="text-center">Featured</h2>
                <div class="separator"></div>
                <?php
                $h1 = \App\Models\Admin\Project::where('id',15)->first();
                $h2 = \App\Models\Admin\Project::where('id',16)->first();
                $h3 = \App\Models\Admin\Project::where('id',17)->first();

                $page5 = \App\Models\Admin\Page::where('name','featured')->first();



                ?>
                <p class="lead text-center">{!! $page5->details !!}</p>
                <div class="col-md-4">
                    <div class="image-box text-center style-2 mb-20">
                        <a href="gallery.html object-non-visible"><img src="{{asset($h1->image)}}" alt="" class="img-circle img-thumbnail" data-animation-effect="zoomIn"></a>
                        <div class="body padding-horizontal-clear">
                            <h4 class="title">{{$h1->name}}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="image-box text-center style-2 mb-20">
                        <a href="gallery.html object-non-visible"><img src="{{asset($h2->image)}}" alt="" class="img-circle img-thumbnail" data-animation-effect="zoomIn"></a>
                        <div class="body padding-horizontal-clear">
                            <h4 class="title">{{$h2->name}}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="image-box text-center style-2 mb-20">
                        <a href="gallery.html object-non-visible"><img src="{{asset($h3->image)}}" alt="" class="img-circle img-thumbnail" data-animation-effect="zoomIn"></a>
                        <div class="body padding-horizontal-clear">
                            <h4 class="title">{{$h3->name}}</h4>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
    <!-- section end -->


@endsection