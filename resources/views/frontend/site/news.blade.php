@extends('frontend.layouts.pak')

@section('content')

    <!-- banner start -->
    <!-- ================ -->

    <!-- banner end -->

    <div id="page-start"></div>

    <!-- section start -->
    <!-- ================ -->
    <section class="video-background" style="margin-top:10px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center" data-animation-effect="zoomIn"
                        data-effect-delay="100">{{$page->title}}</h2>
                    <div class="separator object-non-visible" data-animation-effect="zoomIn"
                         data-effect-delay="100"></div>

                </div>
            </div>
        </div>
    </section>
    <!-- section end -->
    <!-- section start -->
    <!-- ================ -->
    <section class="section clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- -->
                    {!! $page->detail !!}

                </div>
            </div>
        </div>
    </section>
@endsection