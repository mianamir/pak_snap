@extends('frontend.layouts.mrs')

@section('cover')

    <section id="slider-wrapper" class="layer-slider-wrapper layer-slider-fullsize">

        <img src="{{asset($data->image)}}" width="100%" class="img-responsive"/>

    </section>

@endsection



@section('content')


    <section>
        <div class="container">

            <div class="h-nav-main">
                <nav id="menu" class="nav">
                    <button type="button" id="menutoggle" class="navtoogle" aria-hidden="true"><i aria-hidden="true"
                                                                                                  class="icon-menu"> </i>
                        Menu
                    </button>
                    <ul>
                        <li class="h-nav">

                            <a href="{{url('/')}}">
                                        <span class="icon">
                                            <i aria-hidden="true" class="icon-home"></i>
                                        </span>
                                <span>Home</span>
                            </a>

                        </li>


                        <?php
                        $page = \App\Models\Admin\Page::where('name', 'about us')->first();
                        ?>

                        <li class="{{ Active::pattern($page->slug) }} h-nav">
                            <a href="{{route('page',[$page->slug])}}">
                                        <span class="icon">
                                            <i aria-hidden="true" class="icon-services"></i>
                                        </span>
                                <span>About Us</span>
                            </a>
                        </li>


                        <?php
                        $page = \App\Models\Admin\Page::where('name', 'jobs')->first();
                        ?>

                        <li class="{{ Active::pattern($page->slug) }} h-nav">
                            <a href="{{route('page',[$page->slug])}}">
                                        <span class="icon">
                                            <i aria-hidden="true" class="icon-portfolio"></i>
                                        </span>
                                <span>Jobs</span>
                            </a>
                        </li>

                        <?php
                        $page = \App\Models\Admin\Page::where('name', 'services')->first();
                        ?>

                        <li class="{{ Active::pattern($page->slug) }} h-nav">
                            <a href="{{route('page',[$page->slug])}}">
                                        <span class="icon">
                                            <i aria-hidden="true" class="icon-blog"></i>
                                        </span>
                                <span>Services</span>
                            </a>
                        </li>


                        <?php
                        $page = \App\Models\Admin\Page::where('name', 'Procedure')->first();
                        ?>

                        <li class="{{ Active::pattern($page->slug) }} h-nav">
                            <a href="{{route('page',[$page->slug])}}">
                                        <span class="icon">
                                            <i aria-hidden="true" class="icon-contact"></i>
                                        </span>
                                <span>Procedure</span>
                            </a>
                        </li>



                        <?php
                        $page = \App\Models\Admin\Page::where('name', 'contact us')->first();
                        ?>

                        <li class="{{ Active::pattern($page->slug) }} h-nav">
                            <a href="{{route('page',[$page->slug])}}">
                                        <span class="icon">
                                            <i aria-hidden="true" class="icon-team"></i>
                                        </span>
                                <span>Contact Us</span>
                            </a>
                        </li>

                    </ul>
                </nav>
            </div>


            <div class="col-md-3">
                <nav id="menu" class="nav">
                    <ul>


                        <?php
                        $childs = \App\Models\Admin\ChildPages::where('page_id', '=', $data->id)->get();
                        ?>

                        @foreach($childs as $child)
                            <li class="{{ Active::pattern($child->slug) }}" style="width: 100%">
                                <a href="{{route('childpage',[$child->slug])}}">
                                        <span class="icon">
                                            <i aria-hidden="true" class="icon-team-asdad"></i>
                                        </span>
                                    <span>{{ $child->name }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </nav>
            </div>

            <div class="col-md-9">

                <h1> {!! $data->heading !!} </h1>


                {!! $data->details !!}
            </div>
        </div>
    </section>
@endsection