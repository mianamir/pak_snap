@extends('frontend.layouts.pak')

@section('content')
    <section class="pv-30 clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">TERMS</h1>
                    <div class="separator"></div>

                    <p class="text-center">{!! $page->details !!}</p></div>
            </div>
        </div>
    </section>

@endsection