<table class="table table-responsive" id="projects-table">
    <thead>
        <th>Name</th>
        <th>Image</th>

        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php foreach($projects as $project): ?>
        <tr>
            <td><?php echo $project->name; ?></td>
            <td><image src="<?php echo e(asset($project->image)); ?>" width="100" height="100"/></td>

            <td>
                <?php echo Form::open(['route' => ['admin.projects.destroy', $project->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('admin.projects.show', [$project->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('admin.projects.edit', [$project->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>