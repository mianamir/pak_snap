<table class="table table-responsive" id="banners-table">
    <thead>
        <th>Name</th>
        <th>Title</th>
        <th>Created At</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php foreach($banners as $banner): ?>
        <tr>
            <td><?php echo $banner->name; ?></td>
            <td><?php echo $banner->title; ?></td>
            <td><?php echo $banner->created_at; ?></td>
            <td><?php echo $banner->published; ?></td>
            <td>
                <?php echo Form::open(['route' => ['admin.banners.destroy', $banner->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('admin.banners.show', [$banner->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('admin.banners.edit', [$banner->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>