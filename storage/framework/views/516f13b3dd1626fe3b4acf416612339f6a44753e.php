<!-- Name Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('name', 'Name:'); ?>

    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('image', 'Image:'); ?>

    <?php echo Form::file('image'); ?>

</div>
<div class="clearfix"></div>
<div class="form-group col-sm-6">
    <?php echo Form::label('homepage', 'Homepage:'); ?>

    <label class="checkbox-inline">
        <?php echo Form::hidden('homepage', false); ?>

        <?php echo Form::checkbox('homepage', 1, null); ?> yes
    </label>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('admin.projects.index'); ?>" class="btn btn-default">Cancel</a>
</div>
