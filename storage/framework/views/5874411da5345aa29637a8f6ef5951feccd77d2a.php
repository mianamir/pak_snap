<table class="table table-responsive" id="userImages-table">
    <thead>
        <th>Id</th>
        <th>Filename</th>
        <th>Original Filedir</th>
        <th>Original Extension</th>
        <th>Original Width</th>
        <th>Original Height</th>
        <th>Dir</th>
        <th>Original Mime</th>
        <th>Alt Text</th>
        <th>User Id</th>
        <th>Basename</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php foreach($userImages as $userImage): ?>
        <tr>
            <td><?php echo $userImage->id; ?></td>
            <td><?php echo $userImage->filename; ?></td>
            <td><?php echo $userImage->original_filedir; ?></td>
            <td><?php echo $userImage->original_extension; ?></td>
            <td><?php echo $userImage->original_width; ?></td>
            <td><?php echo $userImage->original_height; ?></td>
            <td><?php echo $userImage->dir; ?></td>
            <td><?php echo $userImage->original_mime; ?></td>
            <td><?php echo $userImage->alt_text; ?></td>
            <td><?php echo $userImage->user_id; ?></td>
            <td><?php echo $userImage->basename; ?></td>
            <td>
                <?php echo Form::open(['route' => ['admin.userImages.destroy', $userImage->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('admin.userImages.show', [$userImage->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('admin.userImages.edit', [$userImage->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>