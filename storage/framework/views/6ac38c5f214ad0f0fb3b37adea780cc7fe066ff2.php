<!-- Name Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('name', 'Name:'); ?>

    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('title', 'Title:'); ?>

    <?php echo Form::text('title', null, ['class' => 'form-control']); ?>

</div>

<!-- Detail Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('detail', 'Detail:'); ?>

    <?php echo Form::textarea('detail', null, ['class' => 'form-control']); ?>

</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('image', 'Image:'); ?>

    <?php echo Form::file('image'); ?>

</div>

<div class="form-group col-sm-6">
    <?php echo Form::label('published', 'Published:'); ?>

    <label class="checkbox-inline">
        <?php echo Form::hidden('published', false); ?>

        <?php echo Form::checkbox('published', 1, null); ?> yes
    </label>
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('admin.banners.index'); ?>" class="btn btn-default">Cancel</a>
</div>
