<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo e(access()->user()->picture); ?>" class="img-circle" alt="User Image"/>
            </div><!--pull-left-->
            <div class="pull-left info">
                <p><?php echo e(access()->user()->name); ?></p>
                <!-- Status -->
                <a href="#"><i
                            class="fa fa-circle text-success"></i> <?php echo e(trans('strings.backend.general.status.online')); ?>

                </a>
            </div><!--pull-left-->
        </div><!--user-panel-->

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control"
                       placeholder="<?php echo e(trans('strings.backend.general.search_placeholder')); ?>"/>
                  <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i
                                class="fa fa-search"></i></button>
                  </span>
            </div><!--input-group-->
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header"><?php echo e(trans('menus.backend.sidebar.general')); ?></li>

            <!-- Optionally, you can add icons to the links -->
            <li class="<?php echo e(Active::pattern('admin/dashboard')); ?>">
                <?php echo e(link_to_route('admin.dashboard', trans('menus.backend.sidebar.dashboard'))); ?>

            </li>

            <?php if (access()->allow('manage-users')): ?>
            <li class="<?php echo e(Active::pattern('admin/access/*')); ?>">
                <?php echo e(link_to_route('admin.access.user.index', trans('menus.backend.access.title'))); ?>

            </li>
            <?php endif; ?>

            <?php if (access()->allow('view-backend')): ?>


            <li class="<?php echo e(Active::pattern('admin/banner/*')); ?>">
                <?php echo e(link_to_route('admin.banners.index', "Banners")); ?>

            </li>
            <li class="<?php echo e(Active::pattern('admin/pages/*')); ?>">
                <?php echo e(link_to_route('admin.pages.index', "Pages")); ?>

            </li>

            <li class="<?php echo e(Active::pattern('admin/childPages/*')); ?>">
                <?php echo e(link_to_route('admin.childPages.index', "Child Pages")); ?>

            </li>

            <li class="<?php echo e(Active::pattern('admin/contacts/*')); ?>">
                <?php echo e(link_to_route('admin.contacts.index', "Contact")); ?>

            </li>

            <li class="<?php echo e(Active::pattern('admin/projects/*')); ?>">
                <?php echo e(link_to_route('admin.projects.index', "Project Gallery")); ?>

            </li>

            <li class="<?php echo e(Active::pattern('admin/jobs/*')); ?>">
                <?php echo e(link_to_route('admin.jobs.index', "Jobs")); ?>

            </li>

            <li class="<?php echo e(Active::pattern('admin/quickLinks/*')); ?>">
                <?php echo e(link_to_route('admin.quickLinks.index', "quick Links")); ?>

            </li>

            <li class="<?php echo e(Active::pattern('admin/stories/*')); ?>">
                <?php echo e(link_to_route('admin.stories.index', "stories")); ?>

            </li>




            <?php endif; ?>


            <li class="<?php echo e(Active::pattern('admin/log-viewer*')); ?> ">
                <a href="#">
                    <span><?php echo e(trans('menus.backend.log-viewer.main')); ?></span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu <?php echo e(Active::pattern('admin/log-viewer*', 'menu-open')); ?>"
                    style="display: block; <?php echo e(Active::pattern('admin/log-viewer*', 'display: block;')); ?>">
                    <li class="<?php echo e(Active::pattern('admin/log-viewer')); ?>">
                        <?php echo e(link_to('admin/log-viewer', trans('menus.backend.log-viewer.dashboard'))); ?>

                    </li>
                    <li class="<?php echo e(Active::pattern('admin/log-viewer/logs')); ?>">
                        <?php echo e(link_to('admin/log-viewer/logs', trans('menus.backend.log-viewer.logs'))); ?>

                    </li>
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>