<table class="table table-responsive" id="contacts-table">
    <thead>
    <th>Email</th>
    <th>Phone1</th>
    <th>Phone2</th>
    <th>Address</th>
    <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php foreach($contacts as $contact): ?>
        <tr>
            <td><?php echo $contact->email; ?></td>
            <td><?php echo $contact->phone1; ?></td>
            <td><?php echo $contact->phone2; ?></td>
            <td><?php echo $contact->address; ?></td>
            <td>
                <div class='btn-group'>
                    <a href="<?php echo route('admin.contacts.show', [$contact->id]); ?>" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('admin.contacts.edit', [$contact->id]); ?>" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>