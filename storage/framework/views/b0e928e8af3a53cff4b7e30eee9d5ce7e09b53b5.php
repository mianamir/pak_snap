<?php $__env->startSection('content'); ?>
    <section class="pv-30 clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">Our Profile</h1>
                    <div class="separator"></div>

                    <p class="text-center"><?php echo $page->details; ?></p></div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.pak', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>