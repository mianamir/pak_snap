<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="index, follow">
    <title>Manpower Resource Services</title>

    <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo e(asset('assets/bootstrap/css/bootstrap.min.css')); ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo e(asset('font-awesome/css/font-awesome.min.css')); ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo e(asset('assets/fancybox/jquery.fancybox8cbb.css?v=2.1.5')); ?>" media="screen">

    <link id="wpStylesheet" type="text/css" href="<?php echo e(asset('css/global-style.css')); ?>" rel="stylesheet" media="screen">

    <link href="<?php echo e(asset('images/favicon.png')); ?>" rel="icon" type="image/png">

    <link rel="stylesheet" href="<?php echo e(asset('assets/owl-carousel/owl.carousel.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/owl-carousel/owl.theme.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/sky-forms/css/sky-forms.css')); ?>">


    <script src="<?php echo e(asset('js/jquery.js')); ?>"></script>
    <script src="<?php echo e(asset('js/jquery-ui.min.js')); ?>"></script>

    <link rel="stylesheet" href="<?php echo e(asset('assets/layerslider/css/layerslider.css')); ?>" type="text/css">

    <script type="text/javascript">
        function ShowCurrentTime() {
            var dt = new Date();
            document.getElementById("lblTime").innerHTML = dt.toLocaleTimeString();
            window.setTimeout("ShowCurrentTime()", 1000); // Here 1000(milliseconds) means one 1 Sec
        }
    </script>


    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/default.css')); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/component.css')); ?>"/>
    <script src="<?php echo e(asset('js/modernizr.custom.js')); ?>"></script>

</head>


<body onload="ShowCurrentTime()">
<!-- MODALS -->

<!-- MOBILE MENU - Option 2 -->
<section id="navMobile" class="aside-menu left">
    <form class="form-horizontal form-search">
        <div class="input-group">
            <input type="search" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
                <button id="btnHideMobileNav" class="btn btn-close" type="button" title="Hide sidebar"><i
                            class="fa fa-times"></i></button>
            </span>
        </div>
    </form>
    <div id="dl-menu" class="dl-menuwrapper">
        <ul class="dl-menu"></ul>
    </div>
</section>


<!-- MAIN WRAPPER -->
<div class="body-wrap">

    <!-- HEADER -->
    <div id="divHeaderWrapper">
        <!-- HEADER: Cover example - includes a full page header with with background and semi-opaque navigation with sticky navbar activated -->
        <header class="header-alpha header-cover">
            <!-- Top Header -->
            <div class="top-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                        <span class="aux-text hidden-xs">
                            <?php /*MANPOWER RESOURCE SERVICES</strong> | Overseas Employment Promoter, Licence No. OP&HRD/3696/LHR/2015*/ ?>
                        </span>
                            <nav class="top-header-menu">
                                <ul class="top-menu">
                                    <li>
                                        <a href="#">
                                            <label id="lblTime" style=" font-weight:bold"></label>
                                        </a>

                                    </li>
                                    <li class="aux-languages dropdown animate-hover" data-animate="animated fadeInUp">
                                        <a href="#">
                                            <div id="google_translate_element"></div><script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'ar,en,ur', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
                            }
                        </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                                        </a>
                                        <ul id="auxLanguages" class="sub-menu animate-wr">

                                        </ul>
                                    </li>

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>


            <div class="navbar navbar-wp navbar-fixed mega-nav" data-spy="affix" data-offset-top="120" role="navigation">
                <div class="container">
                    <div class="header-title">
                        <div class="row">

                            <div class="col-md-4">
                                <div class="logo-eng">
                                    <a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('images/logo-eng.png')); ?>" alt="MRS" class="img-responsive"></a>
                                </div>
                            </div> <!--col-md-4 ends-->
                            <div class="col-md-4 mt-15" align="center">
                                <a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('images/mrs-mainlogo.png')); ?>" class="img-responsive"/></a>
                            </div> <!--col-md-4 ends-->
                            <div class="col-md-4">
                                <div class="logo-arabic">
                                    <a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('images/logo-arabic.png')); ?>" /></a>
                                </div>
                            </div>  <!--col-md-4 ends-->

                        </div>
                    </div> <!--row ends-->
                </div>
            </div>
        </header>
    </div>


    <?php echo $__env->yieldContent('cover'); ?>

    <?php echo $__env->yieldContent('content'); ?>


    <footer class="footer">
        <div class="container">
            <div class="row">

                <div class="col-md-7">
                    <div class="col">
                        <h4> MANPOWER RESOURCE SERVICES </h4>
                        <p class="no-margin">
                            <?php
                            $data = \App\Models\Admin\Page::where('name','=','footer')->first()
                            ?>
                            <?php echo $data->details; ?>

                        </p>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="col col-social-icons">
                        <h4>Follow us</h4>
                        <a href="https://www.facebook.com/profile.php?id=100015727487285&ref=bookmarks"><i class="fa fa-facebook" title="MRS | Facebook"></i></a>
                        <a href="https://twitter.com/?request_context=signup"><i class="fa fa-twitter" title="MRS | Twitter"></i></a>
                        <a href="https://www.linkedin.com/in/mrs-manpower-recuritment-9a6b6513a/"><i class="fa fa-youtube-play" title="MRS | Linkedin"></i></a>

                        <!-- hitwebcounter Code START -->
                        <a href="http://www.hitwebcounter.com" target="_blank">
                            <img src="http://hitwebcounter.com/counter/counter.php?page=6677277&style=0009&nbdigits=7&type=page&initCount=100" title="free hits" Alt="free hits"   border="0" >
                        </a>                                        <br/>
                        <!-- hitwebcounter.com --><a href="http://www.hitwebcounter.com" title="Track My Website"
                                                     target="_blank" style="font-family: Geneva, Arial, Helvetica, sans-serif;
                                        font-size: 8px; color: #686A6F; text-decoration: none ;"><strong>Track My Website                                        </strong>
                        </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="col">
                        <h4>Contact us</h4>


                        <?php 
                            $contact = \App\Models\Admin\Contact::orderBy('id','DESC')->first();
                         ?>

                        <ul>
                            <li><?php echo $contact->address; ?></li>
                            <li><strong>Phone:</strong> <?php echo $contact->phone1; ?></li>
                            <li><strong>Mobile:</strong> <?php echo $contact->phone2; ?></li>
                            <li><strong>Fax:</strong> <?php echo $contact->phone3; ?></li>
                            <li><strong>Email:</strong> <a href="mailto:<?php echo $contact->email; ?>"
                                                           title="Email Us"><?php echo $contact->email; ?></a></li>
                        </ul>
                    </div>
                </div>

            </div>

            <hr>

            <div class="row">
                <div class="col-md-12 copyright">
                    2017 © MRS. All rights reserved. |
                    Powered By: <a href="http://www.realwebidea.com" target="_blank">Real Web Idea</a>
                </div>

            </div>
        </div>
    </footer>
</div>

<!-- Essentials -->
<script src="<?php echo e(asset('js/modernizr.custom.js')); ?>"></script>
<script src="<?php echo e(asset('assets/bootstrap/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.mousewheel-3.0.6.pack.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.easing.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.metadata.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.hoverup.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.hoverdir.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery.stellar.js')); ?>"></script>

<!-- Boomerang mobile nav - Optional  -->
<script src="<?php echo e(asset('assets/responsive-mobile-nav/js/jquery.dlmenu.js')); ?>"></script>
<script src="<?php echo e(asset('assets/responsive-mobile-nav/js/jquery.dlmenu.autofill.js')); ?>"></script>

<!-- Forms -->
<script src="<?php echo e(asset('assets/ui-kit/js/jquery.powerful-placeholder.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/ui-kit/js/cusel.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/sky-forms/js/jquery.form.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/sky-forms/js/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/sky-forms/js/jquery.maskedinput.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/sky-forms/js/jquery.modal.js')); ?>"></script>

<!-- Assets -->
<script src="<?php echo e(asset('assets/hover-dropdown/bootstrap-hover-dropdown.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/page-scroller/jquery.ui.totop.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/mixitup/jquery.mixitup.js')); ?>"></script>
<script src="<?php echo e(asset('assets/mixitup/jquery.mixitup.init.js')); ?>"></script>
<script src="<?php echo e(asset('assets/fancybox/jquery.fancybox.pack8cbb.js?v=2.1.5')); ?>"></script>
<script src="<?php echo e(asset('assets/waypoints/waypoints.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/milestone-counter/jquery.countTo.js')); ?>"></script>
<script src="<?php echo e(asset('assets/easy-pie-chart/js/jquery.easypiechart.js')); ?>"></script>
<script src="<?php echo e(asset('assets/social-buttons/js/rrssb.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/nouislider/js/jquery.nouislider.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/owl-carousel/owl.carousel.js')); ?>"></script>
<script src="<?php echo e(asset('assets/bootstrap/js/tooltip.js')); ?>"></script>
<script src="<?php echo e(asset('assets/bootstrap/js/popover.js')); ?>"></script>

<!-- Sripts for individual pages, depending on what plug-ins are used -->
<script src="<?php echo e(asset('assets/layerslider/js/greensock.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/layerslider/js/layerslider.transitions.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/layerslider/js/layerslider.kreaturamedia.jquery.js')); ?>" type="text/javascript"></script>
<!-- Initializing the slider -->
<script>
    jQuery("#layerslider").layerSlider({
        pauseOnHover: true,
        autoPlayVideos: false,
        skinsPath: 'assets/layerslider/skins/',
        responsive: false,
        responsiveUnder: 1280,
        layersContainer: 1280,
        skin: 'borderlessdark3d',
        hoverPrevNext: true,
    });
</script>

<!-- Boomerang App JS -->
<script src="<?php echo e(asset('js/wp.app.js')); ?>"></script>
<!--[if lt IE 9]>
<script src="<?php echo e(asset('js/html5shiv.js')); ?>"></script>
<script src="<?php echo e(asset('js/respond.min.js')); ?>"></script>
<![endif]-->

<!-- Temp -- You can remove this once you started to work on your project -->
<script src="<?php echo e(asset('js/jquery.cookie.js')); ?>"></script>
<script src="<?php echo e(asset('js/wp.switcher.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/wp.ga.js')); ?>"></script>


</body>


</html>