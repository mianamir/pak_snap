<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->

    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo e(date('Y')); ?> <a href="#"><?php echo e(app_name()); ?></a>.</strong>
</footer>